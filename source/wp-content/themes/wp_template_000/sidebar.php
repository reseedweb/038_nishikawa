<div id="nav"><!-- begin nav -->
	<ul>
		<li><a href="<?php bloginfo('url'); ?>/contact">
		<img src="<?php bloginfo('template_url'); ?>/img/common/nav_area_off.png" alt="対応地域　大阪を中心に近畿圏外まで対応可能！　即お見積りのスピード対応　お急ぎの方もお任せください！0120-107-254 06-6328-4800【受付】9:00～18:00（第1・3土曜日・日祝除く）　お問い合わせフォーム" /></a></li>
	</ul>
	
	<dl>
		<dt><img src="<?php bloginfo('template_url'); ?>/img/common/nav_dir_service.gif" alt="サービス一覧" /></dt>
		<dd><a href="<?php bloginfo('url'); ?>/service"><img src="<?php bloginfo('template_url'); ?>/img/common/nav_service_off.gif" alt="オフィス古紙回収" /></a></dd>
		<dd><a href="<?php bloginfo('url'); ?>/service#01"><img src="<?php bloginfo('template_url'); ?>/img/common/nav_service01_off.gif" alt="産業廃棄物収集処理" /></a></dd>
		<dd><a href="<?php bloginfo('url'); ?>/service/oa"><img src="<?php bloginfo('template_url'); ?>/img/common/nav_oa_off.gif" alt="OAサプライリサイクル" /></a></dd>
		<dd><a href="<?php bloginfo('url'); ?>/service/blackbox"><img src="<?php bloginfo('template_url'); ?>/img/common/nav_blackbox_off.gif" alt="BLACK BOX" /></a></dd>
		<dd><a href="<?php bloginfo('url'); ?>/service/blackbox#sutemarukunSec"><img src="<?php bloginfo('template_url'); ?>/img/common/nav_sutemarukun_off.gif" alt="すて丸くん" /></a></dd>
	</dl>
	
	<dl>
		<dt><img src="<?php bloginfo('template_url'); ?>/img/common/nav_dir_for.gif" alt="機密文書・古紙回収をお考えの方へ" /></dt>
		<dd><a href="<?php bloginfo('url'); ?>/benefits"><img src="<?php bloginfo('template_url'); ?>/img/common/nav_benefits_off.gif" alt="古紙・機密文書回収のメリット" /></a></dd>
		<dd><a href="<?php bloginfo('url'); ?>/point"><img src="<?php bloginfo('template_url'); ?>/img/common/nav_point_off.gif" alt="失敗しない業者選びの3つのポイント" /></a></dd>
	</dl>
	
	<dl>
		<dt><img src="<?php bloginfo('template_url'); ?>/img/common/nav_dir_about.gif" alt="株式会社西川について" /></dt>
		<dd><a href="<?php bloginfo('url'); ?>/about"><img src="<?php bloginfo('template_url'); ?>/img/common/nav_about_off.gif" alt="西川が選ばれる5つの安心ポイント" /></a></dd>
		<dd><a href="<?php bloginfo('url'); ?>/cost"><img src="<?php bloginfo('template_url'); ?>/img/common/nav_cost_off.gif" alt="企業様のコストと時間削減事例" /></a></dd>
		<dd><a href="<?php bloginfo('url'); ?>/faq"><img src="<?php bloginfo('template_url'); ?>/img/common/nav_faq_off.gif" alt="よくあるご質問" /></a></dd>
		<dd><a href="<?php bloginfo('url'); ?>/flow"><img src="<?php bloginfo('template_url'); ?>/img/common/nav_flow_off.gif" alt="回収までの流れ" /></a></dd>
		<dd><a href="<?php bloginfo('url'); ?>/environment"><img src="<?php bloginfo('template_url'); ?>/img/common/nav_environment_off.gif" alt="環境保護方針" /></a></dd>
		<dd><a href="<?php bloginfo('url'); ?>/company"><img src="<?php bloginfo('template_url'); ?>/img/common/nav_company_off.gif" alt="会社概要・個人情報保護方針" /></a></dd>
	</dl>
	
	<ul>
		<li><a href="<?php bloginfo('url'); ?>/recruit"><img src="<?php bloginfo('template_url'); ?>/img/common/nav_recruit_off.jpg" alt="採用情報" /></a></li>
		<li><img src="<?php bloginfo('template_url'); ?>/img/common/nav_authentication.gif" alt="安全・安心の証です！ISO 14001 プライバシーマーク取得" /></li>
	</ul>
	
	
	<div class="sidebar-row"><!-- begin sidebar-row-->
		<a href="<?php bloginfo('url'); ?>/#areapoint"><img src="<?php bloginfo('template_url');?>/img/side_maps.jpg" alt="top" /></a>
	</div><!-- end sidebar-row -->

	<div class="sidebar-row"><!-- begin sidebar-row -->
		<?php get_template_part('part','area'); ?>
	</div><!-- end sidebar-row -->


</div><!-- / #nav -->

