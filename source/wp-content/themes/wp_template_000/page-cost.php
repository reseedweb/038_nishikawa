<?php get_header(); ?>
			<h3 class="firstChild"><img src="<?php bloginfo('template_url');?>/img/cost_ti01.png" alt="古紙回収サービスによるコスト・時間削減事例" /></h3>
			<p>株式会社西川の古紙回収サービスをご利用いただいているお客様の、コストと時間の削減事例をご紹介します。ぜひご参考にしてください。</p>
            
            <h4><img src="<?php bloginfo('template_url');?>/img/cost_sti01.png" alt="A税務会計事務所様" width="710" height="39" /></h4>
            <div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/cost_img01.jpg" alt="A税務会計事務所様" width="220" height="177" /></p>
				<p class="last">業務多忙の為、機密文書の処理を全て任せられる大阪の処理会社をインターネットで探されていたところ弊社にお電話頂きました。概算で見積もりを取ってお伺いしたところ、棚に大量の機密文書が収まっていました。多忙の為、ダンボールに箱詰めができなかったとの事でした。弊社で棚出しからご対応させて頂き台車の鉄かごに書類を詰め袋で包みできるだけセキュリティに気を付けてご対応致しました。税理士様からは親切な対応のおかげで作業の手間を削減でき事務所もすっきりしましたと喜んで頂いております。</p>
			</div>
            
            <h4><img src="<?php bloginfo('template_url');?>/img/cost_sti02.png" alt="株式会社F様" width="710" height="39" /></h4>
            <div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/cost_img02.jpg" alt="株式会社F様" width="220" height="150" /></p>
				<p class="last">当ウェブサイトでお付き合いのある会社様です。年度末の大掃除で不要な書類が大量に出たので回収して欲しいとご連絡がありました。会社も近くにあるのでトラックにて訪問致しました。ダンボールに箱詰めされた書類を台車で運んでトラックに運んで問題なく回収致しました。担当者の方からは西川さんに依頼するとファイルなどを分別しなくて良いから手間が省ける上にとても安いので助かりますと声を頂いています。</p>
			</div>					
			<p class="contentBnr last"><a href="<?php bloginfo('url');?>/contact"><img src="<?php bloginfo('template_url');?>/img/common/contentbnr_fmail_off.jpg" alt="お電話・FAX・メールにてお気軽にお問い合わせ下さい！0120-107-254 06-6328-4800 FAX06-6327-1104 【受付】9:00～18:00（第1・3土曜日・日祝除く）　スピード無料見積 お申し込み受付中！" /></a></p>	
<?php get_footer(); ?>