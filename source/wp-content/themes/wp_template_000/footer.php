					</div><!-- end content -->					
					<aside id="sidebar"><!-- begin sidebar -->
                        <?php if(is_page('blog') || is_category() || is_single()) : ?>
                            <?php
                            $queried_object = get_queried_object();                                
                            $sidebar_part = 'blog';
                            if(is_tax() || is_archive()){                                    
                                $sidebar_part = '';
                            }                               

                            if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
                                $sidebar_part = '';
                            }   
                                    
                            if($queried_object->taxonomy == 'category'){                                    
                                $sidebar_part = 'blog';
                            }                 
                            ?>
                            <?php get_template_part('sidebar',$sidebar_part); ?>  
                        <?php else: ?>
                            <?php get_template_part('sidebar'); ?>  
                        <?php endif; ?>                                    
                    </aside><!-- end sidebar -->  				
				</div><!-- end main -->
			</div><!-- end wrapper -->
			<footer id="footer"><!-- begin footer -->
				<div id="footInner" class="cf">
					<p id="footLogo"><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/img/common/footer_logo.gif" alt="" /></a></p>
					<ul id="footerLink">
						<li class="firstChild"><a href="<?php bloginfo('url'); ?>">古紙回収・機密文書処理なら株式会社西川</a></li>
						<li><a href="<?php bloginfo('url'); ?>/service">オフィス古紙回収</a></li>
						<li><a href="<?php bloginfo('url'); ?>/service/oa/">OAサプライリサイクル</a></li>
						<li><a href="<?php bloginfo('url'); ?>/service/blackbox">すて丸くん・BLACKBOX</a></li>
						<li class="firstChild"><a href="<?php bloginfo('url'); ?>/benefits">古紙・機密文書回収のメリット</a></li>
						<li><a href="<?php bloginfo('url'); ?>/point">失敗しない業者選びの3つのポイント</a></li>
						<li><a href="<?php bloginfo('url'); ?>/about">西川が選ばれる5つの安心ポイント</a></li>
						<li class="firstChild"><a href="<?php bloginfo('url'); ?>/cost">企業様のコストと時間削減事例</a></li>
						<li><a href="<?php bloginfo('url'); ?>/faq">よくあるご質問</a></li>
						<li><a href="<?php bloginfo('url'); ?>/flow">回収までの流れ</a></li>
						<li><a href="<?php bloginfo('url'); ?>/recruit">採用情報</a></li>
						<li><a href="<?php bloginfo('url'); ?>/environment">環境保護方針</a></li>
						<li><a href="<?php bloginfo('url'); ?>/message">企業理念・代表挨拶</a></li>
						<li class="firstChild"><a href="<?php bloginfo('url'); ?>/company">会社概要・個人情報保護方針</a></li>
						<li><a href="<?php bloginfo('url'); ?>/contact">お問い合わせ・お見積もり</a></li>
					</ul>
					<p id="copyright">Copyright &copy; Nishikawa Co.,Ltd. All Rights Reserved.</p>
				</div><!-- end footInner -->
			</footer><!-- end footer -->
		</div><!-- end container -->
<?php wp_footer();?>
	</body><!-- end indexPage -->
</html>    