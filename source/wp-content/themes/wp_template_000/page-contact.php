<?php get_header(); ?>
<div style="display:none;">
	<select id="_selectarea_content">
		<option value="" selected="selected">【選択して下さい】</option>
		<optgroup label="北海道">
		<option value="北海道">北海道</option>
		</optgroup>
		<optgroup label="東北">
		<option value="青森県">青森県</option>
		<option value="岩手県">岩手県</option>
		<option value="宮城県">宮城県</option>
		<option value="秋田県">秋田県</option>
		<option value="山形県">山形県</option>
		<option value="福島県">福島県</option>
		</optgroup>
		<optgroup label="関東">
		<option value="茨城県">茨城県</option>
		<option value="栃木県">栃木県</option>
		<option value="群馬県">群馬県</option>
		<option value="埼玉県">埼玉県</option>
		<option value="千葉県">千葉県</option>
		<option value="東京都">東京都</option>
		<option value="神奈川県">神奈川県</option>
		</optgroup>
		<optgroup label="北陸">
		<option value="富山県">富山県</option>
		<option value="石川県">石川県</option>
		<option value="福井県">福井県</option>
		</optgroup>
		<optgroup label="甲信越">
		<option value="新潟県">新潟県</option>
		<option value="山梨県">山梨県</option>
		<option value="長野県">長野県</option>
		</optgroup>
		<optgroup label="東海">
		<option value="岐阜県">岐阜県</option>
		<option value="静岡県">静岡県</option>
		<option value="愛知県">愛知県</option>
		<option value="三重県">三重県</option>
		</optgroup>
		<optgroup label="関西">
		<option value="滋賀県">滋賀県</option>
		<option value="京都府">京都府</option>
		<option value="大阪府">大阪府</option>
		<option value="兵庫県">兵庫県</option>
		<option value="奈良県">奈良県</option>
		<option value="和歌山県">和歌山県</option>
		</optgroup>
		<optgroup label="中四国">
		<option value="鳥取県">鳥取県</option>
		<option value="島根県">島根県</option>
		<option value="岡山県">岡山県</option>
		<option value="広島県">広島県</option>
		<option value="山口県">山口県</option>
		<option value="徳島県">徳島県</option>
		<option value="香川県">香川県</option>
		<option value="愛媛県">愛媛県</option>
		<option value="高知県">高知県</option>
		</optgroup>
		<optgroup label="九州">
		<option value="福岡県">福岡県</option>
		<option value="佐賀県">佐賀県</option>
		<option value="長崎県">長崎県</option>
		<option value="熊本県">熊本県</option>
		<option value="大分県">大分県</option>
		<option value="宮崎県">宮崎県</option>
		<option value="鹿児島県">鹿児島県</option>
		</optgroup>
		<optgroup label="沖縄">
		<option value="沖縄県">沖縄県</option>
		</optgroup>
	</select>
</div>
	<h3 class="firstChild"><img src="<?php bloginfo('template_url');?>/img/fmail_ti01.png" alt="お気軽にお問い合わせください" /></h3>
	<p>当社へのお問い合わせ・お見積もりのご依頼は、お電話もしくは下記メールフォームよりお願いします。お見積もりは無料。ご依頼をいただいてから1営業日中にお見積もりを作成しますので、お気軽にお問い合わせください。</p>
	<p class="last"><img src="<?php bloginfo('template_url');?>/img/form_img.jpg" alt="お電話のご対応でも丁寧にご" width="710" height="150" border="0" /></p>
		<?php echo do_shortcode('[contact-form-7 id="248" title="お問い合わせ"]') ?>
			<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
			<script type="text/javascript">
				$(document).ready(function(){
					$('#zip').change(function(){					
						//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
	    				AjaxZip3.zip2addr(this,'','pref','your-city');
	  				});
				});
			</script>
	<div class="policy">
		<p class="serviceTi">個人情報保護方針</p>
		<p>株式会社西川（以下「当社」）は、機密書類処理および産業廃棄物処理関連業務において質の高いサービスを提供することにより、お客様の信頼にお応えするとともに、業務の適正な運営と健全な業績の伸長を通して広く、社会・経済の発展に貢献することを経営理念としております。</p>
		<p>お客様の情報を安全に保管・管理・処理し、以下に掲げた事項を常に念頭に置き、お客様の個人情報保護に万全を尽くしてまいります。</p>
		<ol class="numList">
			<li>当社は、個人の尊厳を重んじ、個人情報に関して適用される法令等と社会秩序を遵守のうえ、お客様個人に関する情報（以下「個人情報」）の取り扱いについて社内の規程類に則り、個人情報の適切な保護に努めます。</li>
			<li>当社は、お客様よりお預かりした個人情報について、厳正なる管理を行い、お知らせした目的の範囲内で利用します。また個人情報の処理を外部へ委託する場合には、漏洩等を行わないよう契約により義務づけ、適切な管理を実施させていただきます。</li>
			<li>当社が保有する個人情報について、正確かつ最新の状態に保ち、個人情報への不正アクセス、紛失、破壊・改ざんおよび漏洩等の予防ならびに是正に関する処置を講じます。</li>
			<li>当社は、取得した個人情報の取り扱いの全部または一部を委託する場合、十分な保護水準を満たした業者を選定し、契約等により適切な措置を講じます。</li>
			<li>当社は、お客様から個人情報を収集させていただく場合は、収集目的、お客様に対する当社の窓口をお知らせしたうえで、必要な範囲で個人情報を収集させていただきます。また、お客様の個人情報を収集目的の範囲内で利用するとともに、適切な方法で管理し、お客様の承諾なく第三者に開示・提供することはありません。</li>
			<li>当社は、保有する個人情報保護のための社内体制を整備し、これを実効あるものとして運用するとともに必要に応じて継続的改善に努めます。</li>
			<li>当社の個人情報の取り扱いに関して、ご本人からの苦情・相談につきましては、適切かつ迅速に対応し、ご本人から個人情報の開示等のご要望について法令等に従い、遅滞なく対応します。</li>
		</ol><!-- end numList -->
		<ul class="asterisk">
			<li>※この方針は、｢国が定める指針その他の規範」を遵守し、お客様のみならず、当社の役員および従業員と関係あるすべての個人情報についても、上記と同様に取り扱います。</li>
		</ul><!-- end asterisk -->

		<div class="greenFrameBox customerService">
			<div class="greenFrameInner">
			<p class="ti">【お客様相談窓口】</p>
			<p class="last">顧問　野島清<br />
			大阪府大阪市東淀川区大隅1丁目5番23号<br />
			TEL：06-6328-4800</p>
			</div>
		</div><!-- end greenFrameBox -->
		<p class="next">個人情報保護方針は社内外に公表する。</p>
		<div class="signBox over">
			<p class="imgL">制定日 2004年10月1日<br />
			改訂日 2009年7月7日</p>
			<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/company_sign.gif" alt="株式会社　西川　代表取締役　西川滋夫" /></p>
		</div><!-- end signBox -->
		<p class="serviceTi">個人情報の取り扱いについて</p>
		<h4 class="capBg">1.当社の名称</h4>
		<p class="last">株式会社　西川</p>
		<h4 class="capBg">2.個人情報保護管理者および個人情報問い合わせ窓口</h4>
		<p class="last">顧問　野島清<br />
		大阪府大阪市東淀川区大隅1丁目5番23号<br />
		TEL：06-6328-4800<br />
		FAX：06-6328-4802<br />
		E-MAIL：<a href="mailto:privacy@nishikawa.com">privacy@nishikawa.com</a></p>
		<h4 class="capBg">3.個人情報の利用目的の公表</h4>
		<p>当社が、書面で本人より直接取得する場合および間接的に取得する場合の個人情報、ならびに個人データの利用目的は、以下の通りです。</p>
		<h5>（1）従業員情報</h5>
		<div class="numSection">
			<p class="last">当社従業員の雇用・人事管理上必要なため、ならびに募集採用・退職者からの問い合わせのため</p>
		</div>
		<h5>（2）お客様からお預かりした機密書類</h5>
		<div class="numSection">
			<p class="last">溶解リサイクルのため<br />
			※開示対象につきましては（1）のみとなります</p>
		</div>
		<h4 class="capBg">4.個人情報の開示および訂正・追加または削除についての手続き</h4>
		<p class="last">当社では、本人または代理人からの開示の求め、および訂正・追加または削除の求めに対応させていただいております。</p>
		<h5>（1）開示等の求めの申出先</h5>
		<div class="numSection">
			<p>開示等の求めについては、2に記載する窓口にお電話をいただければ、10営業日以内に対応いたします。開示の結果、誤った情報があり、訂正または削除が必要であれば、速やかに対応し、本人にその旨を通知いたします。</p>
		</div>
		<h5>（2）開示等の求めに際しての本人確認の方法</h5>
		<div class="numSection">
			<p class="last">来社の場合は、運転免許証、健康保険被保険証のいずれか現住所が記載されているものをご提示いただき、本人の確認をいたします。また、個人情報開示申請書をご記入・ご提出いただきます。お電話の場合は、本人の氏名・住所・生年月日をお伝えいただきます。</p>
		</div>
		<h5>（3）代理人による開示等の求め</h5>
		<div class="numSection">
			<p class="last">開示等の求めをする者が未成年者または成年被後見人の法定代理人もしくは開示等の求めをすることにつき本人が委任した代理人である場合は、委任状をご提出いただきます。</p>
		</div>
		<h5>（4）手続きに要する費用について</h5>
		<div class="numSection">
			<p class="last">郵送の場合は、郵便・宅配にかかる実費費用をいただきます。また、「利用目的の通知」「開示」を請求される場合は、手数料として1,000円をいただきます。</p>
		</div>
		<h5>（5）認定個人情報保護団体の名称および苦情の解決の申し出先</h5>
		<div class="numSection">
			<p class="last">当社は次の認定個人情報保護団体の対象事業者となっています。<br />
			名称：<a href="http://privacymark.jp/protection_group/" target="_blank">一般財団法人　日本情報経済社会推進協会</a></p>
		</div>
	</div><!-- end policy -->
<?php get_footer(); ?>