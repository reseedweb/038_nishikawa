<!DOCTYPE html>
<html>
    <head>
        <!-- meta -->        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- title -->
        <title><?php if(is_home()){ echo bloginfo("name"); echo " | "; echo bloginfo("description"); } else { echo wp_title(" | ", false, right); echo bloginfo("name"); } ?> </title>
        <meta name="robots" content="noindex,follow,noodp" />
        
        <link rel="profile" href="http://gmpg.org/xfn/11" />                
        
        <!-- global javascript variable -->
        <script type="text/javascript">
            var CONTAINER_WIDTH = '990px';
            var CONTENT_WIDTH = '960px';
            var BASE_URL = '<?php bloginfo('url'); ?>';
            var TEMPLATE_URI = '<?php bloginfo('template_url') ?>';
            var CURRENT_MODULE_URI = '';
            Date.now = Date.now || function() { return +new Date; };            
        </script>        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
        <![endif]-->        
        
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.plugins.js" type="text/javascript"></script>               
        <link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />
        <script src="<?php bloginfo('template_url'); ?>/js/config.js" type="text/javascript"></script>        
		
		<script src="<?php bloginfo('template_url'); ?>/js/rollover.min.js" type="text/javascript"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/current.js" type="text/javascript"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/page-scroller.js" type="text/javascript"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/imagegallery.js" type="text/javascript"></script>
		
        <?php wp_head(); ?>
    </head>
    <body><!-- begin indexPage -->
		<div id="container"><!-- begin container -->			
				<header id="header" ><!-- begin header -->
					<div class="header-bg wrapper clearfix">
						<div class="header-left">
							<h1 class="top-text">機密文書・書類の溶解処理・廃棄なら大阪にある株式会社西川へ</h1>
							<p class="logo"><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/img/common/logo.gif" alt="古紙回収・機密文書処理なら株式会社西川" /></a></p>
						</div>
						
						<div class="header-right">
							<ul id="guideNav">
								<li><a href="<?php bloginfo('url'); ?>/environment"><img src="<?php bloginfo('template_url'); ?>/img/common/header_guidenav_environment_off.gif" alt="環境保護方針" /></a></li>
								<li><a href="<?php bloginfo('url'); ?>/company#privacySec"><img src="<?php bloginfo('template_url'); ?>/img/common/header_guidenav_privacy_off.gif" alt="個人情報保護方針" /></a></li>
							</ul>
							<p id="headInfo"><img src="<?php bloginfo('template_url'); ?>/img/common/header_info.gif" alt="0120-107-254 06-6328-4800 FAX.06-6327-1104 【受付】9:00～18:00（第1・3土曜日・日祝除く）" /></p>
						</div>											
						
						<div id="header-human"><img src="<?php bloginfo('template_url'); ?>/img/common/header_human.jpg" alt="top" /></div>
					</div>				
				</header><!-- end header -->
				
				
				<?php get_template_part('part','navi'); ?>		
								

				<?php if(is_home()) : ?>
					<?php get_template_part('part','topbanner'); ?>	
				<?php else :?>			
					<?php get_template_part('part','contentbanner'); ?>	
				<?php endif; ?>			
			<div id="wrapper" class="clearfix"><!-- begin wrapper -->
				<div id="main"><!-- begin main --> 
					<div id="content"><!-- begin content -->