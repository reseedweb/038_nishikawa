/* ------------------------------------------------------- */
/* Update：2010.03.03
/* 現在位置のナビゲーションを反転させる為のjsファイル
/* ------------------------------------------------------- */

$(document).ready( function() {
		$("#globalNav li a,#nav a").each( function() {
				var url = document.URL.split("#");
				if ( this == url[0] || this + "index.html" == url[0]) {
						$(this).addClass("current");
				}
		});
		
		$('.mailform select[name="selectarea"]').html($("#_selectarea_content").html());
		
		$('#btnback').click(function() {			
			$('input[type="email"]').each(function() {				
				$( ".error" ).addClass( "errorclr" );	
			});
		});		
});
$(function() {
  $(".current").each(function(){
		$(this).find('img').each(function(){
			this.currentSrc = this.getAttribute('src').replace("_off.", "_on.");
			$(this).attr('src',this.currentSrc);
		});
	});
	
	$("#accordion #area-content").hide();				
	$("#accordion h4").click(function(){				
		$accordion = $(this).next();				
		if ($accordion.is(':hidden') === true) {
			$("#accordion #area-content").slideUp();
			$accordion.slideDown();
		} else {
			$accordion.slideUp();
		}
	});
				
});
