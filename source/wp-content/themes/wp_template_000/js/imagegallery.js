/* ------------------------------------------------------- */
/* Update：2011.12.21
/* 画像ギャラリー
/* ------------------------------------------------------- */

$(function(){
	$(".galleryBox").each(function(){
		var zoomImg = $(".zoomImg",this);
		
		// サムネイルをクリックした時
		$(".thumbBox a",this).click(function(){
			clearInterval(timer);
			var targetImg = $(this).attr("href");
			zoomImg.css("opacity",0).attr("src",targetImg).fadeTo(500,1);
			return false;
		});
		
		// 自動再生
		var i = 1;
		var thumbs = $(".thumbBox a",this);
		$("img",thumbs[0]).css("opacity",0.5);
		
		var timer = setInterval(function(){autoChangeImage();},3000);
		function autoChangeImage(){
			if(i>thumbs.length-1) return;
			var targetImg = $(thumbs[i]).attr("href");
			zoomImg.css("opacity",0).attr("src",targetImg).fadeTo(500,1);
			// サムネイル
			$("img",thumbs).css("opacity",1);
			$("img",thumbs[i]).css("opacity",0.5);
			i++;
		}
		
	});
});