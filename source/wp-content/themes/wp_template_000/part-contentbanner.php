					<section id="feature">
						<div class="wrapper clearfix">        
							<div class="header-content clearfix"> 
								<img alt="top" src="<?php bloginfo('template_url'); ?>/img/common/top_content_banner.jpg" />							
								<?php if(is_page('company')) : ?>																				
									<h2 class="feature-title"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/company_pageti.jpg" /></h2>											
								<?php elseif(is_page('faq')) : ?>																				
									<h2 class="feature-title"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/faq_pageti.jpg" /></h2>
								<?php elseif(is_page('flow')) : ?>																				
									<h2 class="feature-title"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/flow_pageti.jpg" /></h2>
								<?php elseif(is_page('about')) : ?>																				
									<h2 class="feature-title"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/about_pageti.jpg" /></h2>
								<?php elseif(is_page('message')) : ?>																				
									<h2 class="feature-title"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/message_pageti.jpg" /></h2>											
								<?php elseif(is_page('environment')) : ?>																				
									<h2 class="feature-title"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/environment_pageti.jpg" /></h2>											
								<?php elseif(is_page('service')) : ?>																				
									<h2 class="feature-title"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/service_pageti.jpg" /></h2>
								<?php elseif(is_page('benefits')) : ?>																				
									<h2 class="feature-title"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/benefits_pageti.jpg" /></h2>
								<?php elseif(is_page('point')) : ?>																				
									<h2 class="feature-title"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/point_pageti.jpg" /></h2>
								<?php elseif(is_page('cost')) : ?>																				
									<h2 class="feature-title"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/cost_pageti.jpg" /></h2>
								<?php elseif(is_page('recruit')) : ?>																				
									<h2 class="feature-title"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/recruit_pageti.jpg" /></h2>
								<?php elseif(is_page('oa')) : ?>																				
									<h2 class="feature-title"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/oa_pageti.jpg" /></h2>
								<?php elseif(is_page('blackbox')) : ?>																				
									<h2 class="feature-title"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/blackbox_pageti.jpg" /></h2>
								<?php elseif(is_page('contact')) : ?>																				
									<h2 class="feature-title"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/fmail_pageti.jpg" /></h2>
								<?php else :?>
									<h2 class="feature-title-text"><?php the_title(); ?></h2>							
								<?php endif; ?>
							</div><!-- ./header-content -->
						</div><!-- ./wrapper -->
					</section><!-- end feature -->
					<section id="page-breadcrumb">
						<div class="wrapper clearfix">
							<?php if( !is_front_page() && function_exists('bcn_display')) : ?>    								
									<?php bcn_display(); ?> 								
							<?php endif; ?>
						</div><!-- wrapper -->
					</section><!-- end page-breadcrumb -->