<?php get_header(); ?>
			<h3 class="firstChild"><img src="<?php bloginfo('template_url');?>/img/benefits_ti01.png" alt="古紙・機密文書の回収が注目されている理由" /></h3>
			<p class="last">古紙や機密文書の回収、溶解・廃棄処理を業者に任せることの一番のメリットは、安全かつ手間がほとんどかからないこと。機密文書等を社内で廃棄するため大量のシュレッダーをかける場合と比べて、時間や人件費などのコストを約90％もカットできます。また、機密文書を溶解・廃棄処理する手間・コスト削減の他にもメリットはさまざま。不況や環境破壊が叫ばれる今、多くの企業が古紙・機密文書の回収、溶解・廃棄処理を採用しています。</p>
			<table cellspacing="0" cellpadding="0" class="type04 next">
				<col width="33%" />
				<col width="33%" />
				<col width="33%" />
				<tr>
					<th><img src="<?php bloginfo('template_url');?>/img/benefits_point01.gif" alt="環境にやさしい" /></th>
					<th><img src="<?php bloginfo('template_url');?>/img/benefits_point02.gif" alt="セキュリティー性に優れている" /></th>
					<th><img src="<?php bloginfo('template_url');?>/img/benefits_point03.gif" alt="企業のイメージアップにつながる" /></th>
					</tr>
				<tr>
					<td>古紙・機密文書の回収、溶解・廃棄処理による紙のリサイクルは、森林伐採などの環境破壊に対する、現時点での非常に有効な対策といえます。また、木材をパルプ化して製造する場合に比べて、総CO2排出量を削減できます。</td>
					<td>古紙・機密文書は回収された後、一般的に製紙工場へと運ばれ、そこで溶解・廃棄処理が施されます。溶解・廃棄処理は機密文書の情報漏洩の心配がないセキュリティー性に優れた方法です。また、溶解・廃棄処理の際は溶解証明書が発行されます。</td>
					<td>環境保全が社会的に注目されているなかで、環境にやさしい古紙・機密文書の回収、溶解・廃棄処理や再生紙の利用を積極的に採用することで、企業のイメージアップにつながります。</td>
					</tr>
			</table>
			<p class="next center"><img src="<?php bloginfo('template_url');?>/img/common/arrow_green.png" alt="benefits" /></p>
			<p class="next center last"><img src="<?php bloginfo('template_url');?>/img/benefits_copy01.gif" width="504" height="52" alt="古紙・機密文書の回収なら西川へ　御社の”古紙の循環リサイクル化”をサポートします" /></p>
			<h4><img src="<?php bloginfo('template_url');?>/img/benefits_sti01.png" alt="西川がご提案する「古紙の循環リサイクル」" /></h4>
			<p class="last">「すでにオフィスで使用している古紙・機密文書をリサイクルしている」という企業様は少なくないかもしれませんが、当社がご提案しているのは、従来のリサイクルからさらに一歩進んだリサイクル。「自分で出した古紙・機密文書を回収、溶解・廃棄処理したうえで、再利用して自分で使う」という“古紙の循環リサイクル化”をご提案しています。</p>
			<h5 class="capBg"><span>リサイクルトイレットペーパーを販売しています</span></h5>
			<div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/benefits_img01.png" alt="benefits" /></p>
				<p class="last">当社では、回収した古紙・機密文書を溶解・廃棄処理してから生まれたリサイクルトイレットペーパーの販売を行っています。パルプを使用したトイレットペーパーよりも安価な上記リサイクルトイレットペーパーを、古紙・機密文書の回収時にお届けすることで運搬費を軽減し、さらに安価で納品。エコと経費削減を同時に可能にします。古紙・機密文書の回収、溶解・廃棄処理にご興味のある方は、お気軽にお問い合わせください。</p>
			</div>
			
			<h5 class="capBg"><span>リサイクルトイレットペーパーの特長</span></h5>
			<ul class="iconList">
				<li>もちろん古紙100％でとってもエコ </li>
				<li>古紙・機密文書を回収、溶解・廃棄処理したうえで、回収時の納品にて運搬費を軽減</li>
				<li>ごみを減らせる「芯なしタイプ」、交換の手間が減る「業務用ロングタイプ」をご用意 </li>
				<li>御社だけのオリジナルトイレットペーパーの作成も可能</li>
			</ul>
			
			<p class="contentBnr last"><a href="<?php bloginfo('url');?>/about"><img src="<?php bloginfo('template_url');?>/img/common/bnr_about_off.png" alt="古紙回収・リサイクル業を続けて60年以上 西川が選ばれる5つの安心ポイントはこちら" /></a></p>
			<p class="contentBnr last"><a href="<?php bloginfo('url');?>/contact"><img src="<?php bloginfo('template_url');?>/img/common/contentbnr_fmail_off.jpg" alt="お電話・FAX・メールにてお気軽にお問い合わせ下さい！0120-107-254 06-6328-4800 FAX06-6327-1104 【受付】9:00～18:00（第1・3土曜日・日祝除く）　スピード無料見積 お申し込み受付中！" /></a></p>	
<?php get_footer(); ?>