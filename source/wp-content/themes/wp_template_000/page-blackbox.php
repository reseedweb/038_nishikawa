<?php get_header(); ?>
				<ul class="anchorList2 cf">
				<li><a href="#sutemarukunSec"><img src="<?php bloginfo('template_url');?>/img/blackbox_anchor_sutemaru_off.jpg" alt="すて丸くん" /></a></li>
				<li><a href="#blackboxSec"><img src="<?php bloginfo('template_url');?>/img/blackbox_anchor_blackbox_off.jpg" alt="BLACKBOX" /></a></li>
			</ul>
			
			<h3 id="sutemarukunSec"><img src="<?php bloginfo('template_url');?>/img/blackbox_ti01.png" alt="すて丸くん" /></h3>
			<p class="last">シュレッダーのような形状の機密文書用回収箱を使用したサービス「すて丸くん」。回収箱・プラン・費用ともに、お客様の用途に合わせてお選びいただけるサービスとなっています。</p>
			
			<h4><img src="<?php bloginfo('template_url');?>/img/blackbox_sti01.png" alt="サービス詳細" /></h4>
			<h5 class="capBg firstChild"><span>回収箱について</span></h5>
			<table cellspacing="0" cellpadding="0" class="type02">
				<col width="33%" />
				<col width="33%" />
				<col width="33%" />
				<tr>
					<th>DSパックI</th>
					<th>DSパックII</th>
					<th>その他の箱 </th>
					</tr>
				<tr>
					<td class="imgCel"><img src="<?php bloginfo('template_url');?>/img/blackbox_boximg01.jpg" alt="" /></td>
					<td class="imgCel"><img src="<?php bloginfo('template_url');?>/img/blackbox_boximg02.jpg" alt="" /></td>
					<td class="imgCel">&nbsp;</td>
					</tr>
				<tr>
					<td>容量20kg（ハードケースなし） </td>
					<td>容量25kg（ハードケースあり） </td>
					<td class="nonCel">-</td>
					</tr>
				<tr>
					<td>期限付き保存書類やスポット的な大量書類の廃棄に適した回収箱。 </td>
					<td>日常業務で発生する廃棄書類をそのまま投入する一時保管型回収箱。中箱を未開封のままリサイクル処理します。また、ハードケース（オプション）の使用によりセキュリティー性が向上します。 </td>
					<td>すて丸くんサービスをご利用のお客様に限り、当社指定の保存箱以外の箱（ダンボール箱など）での処分にも対応します。 </td>
					</tr>
			</table>
			<ul class="asterisk">
				<li>※ハードケースは買い取りもしくはレンタルになります</li>
			</ul>
			<h5 class="iconSquare"><span>回収プランについて</span></h5>
			<table cellspacing="0" cellpadding="0">
				<col width="150" />
				<col />
				<tr>
					<th>予約回収 </th>
					<td>お客様からのご連絡を受けて回収に伺います。 </td>
					</tr>
				<tr>
					<th>おまかせ回収 </th>
					<td>一定周期で回収に伺います。 </td>
					</tr>
			</table>
			<h5 class="iconSquare"><span>回収費用について</span></h5>
			<table cellspacing="0" cellpadding="0">
				<col width="150" />
				<col />
				<tr>
					<th>Aプラン</th>
					<td>ハードケースを設置されているお客様</td>
					</tr>
				<tr>
					<th>Bプラン</th>
					<td>ハードケースを設置されていないお客様</td>
					</tr>
			</table>
			
			<p class="last next">AプランはBプランよりも割安なプランとなり、社内にひとつでもハードケースを設置されている場合は、その他の回収箱についてもAプランでのサービス料金にて回収いたします。なお、回収単価は地域や回収頻度によって異なりますので、詳しくはお気軽にお問い合わせください。</p>
			
			<h4><img src="<?php bloginfo('template_url');?>/img/blackbox_sti02.png" alt="すて丸くんの5つの特長" /></h4>
			<div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/blackbox_illust01.png" alt="" /></p>
				<div class="wrap">
					<h5 class="firstChild"><img src="<?php bloginfo('template_url');?>/img/blackbox_sutemaru_feature01.jpg" alt="1.書類の廃棄は回収箱に投入するだけ" /></h5>
					<p class="last">お届けした回収箱（DSパックI・DSパックII）に書類を投入するだけで機密文書を廃棄できます。シュレッダーの煩わしい作業は必要ありません。</p>
					</div>
				<!-- / .cf --></div>
			<div class="cf next">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/blackbox_illust02.png" alt="2.セキュリティー性が高く安全" width="138" height="124" /></p>
				<div class="wrap">
					<h5 class="firstChild"><img src="<?php bloginfo('template_url');?>/img/blackbox_sutemaru_feature02.jpg" alt="2.セキュリティー性が高く安全" /></h5>
					<p class="last">セキュリティー性の高いサービスをご提供するため、ボックスに密閉シールを貼った後、当社スタッフが回収から製紙会社（溶解処理場）への運搬までを責任を持って行います。委託業者による運搬誤送の心配はありません。</p>
					</div>
				<!-- / .cf next --></div>
			<div class="cf next">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/blackbox_illust03.png" alt="" /></p>
				<div class="wrap">
					<h5 class="firstChild"><img src="<?php bloginfo('template_url');?>/img/blackbox_sutemaru_feature03.jpg" alt="3.万全の体制を整備" /></h5>
					<p class="last">運搬中の回収車にはGPS（位置情報システム）とWebカメラを搭載しており、車内は弊社で監視しています。また、重要文書はその日のうちに溶解処理、チャーター便のみ未開封状態のまま溶解処理槽に投入される様子をWeb上でご確認いただけます。(※Webカメラご使用の際はご予約が必要となります。)</p>
					</div>
				<!-- / .cf next --></div>
			<div class="cf next">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/blackbox_illust04.png" alt="" /></p>
				<div class="wrap">
					<h5 class="firstChild"><img src="<?php bloginfo('template_url');?>/img/blackbox_sutemaru_feature04.jpg" alt="4.補償制度あり" /></h5>
					<p class="last">万が一、回収してから処理するまでの間に文書内容が漏洩し、お客様に損害がおよんだ場合には、最高5億円の補償をさせていただきます。</p>
					</div>
				<!-- / .cf next --></div>
			<div class="cf next">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/blackbox_illust05.png" alt="" /></p>
				<div class="wrap">
					<h5 class="firstChild"><img src="<?php bloginfo('template_url');?>/img/blackbox_sutemaru_feature05.jpg" alt="5.環境保全に貢献可能" /></h5>
					<p class="last">回収した機密文書はその日のうちに溶解処理を行います。焼却をしないためCO2の発生を防ぐことができるうえ、トイレットペーパーとして100％再資源化でき、環境保全に大きく貢献できます。</p>
					</div>
				<!-- / .cf next --></div>
                
                <h4><img src="<?php bloginfo('template_url');?>/img/blackbox_sti07.png" alt="お問い合わせからの流れ" width="710" height="39" /></h4>
                <div class="orangeFrameBox">
				<div class="orangeFrameInner cf">

					<p class="imgL"><img src="<?php bloginfo('template_url');?>/img/common/icon_number01.png" alt="1" width="30" height="33" /></p>
					<div class="wrap">
						<p>お問い合わせ</p>
						<p class="last">「すて丸くん」の導入を検討されている方は、お気軽にご連絡ください。<br />
【お電話】　06-6328-4800<br />
【FAX】　06-6328-4802</p>
				  </div>
				  </div>
				<!-- / .orangeFrameBox --></div>
                
                <div class="orangeFrameBox next">
				<div class="orangeFrameInner cf">
					<p class="imgL"><img src="<?php bloginfo('template_url');?>/img/common/icon_number02.png" alt="2" width="30" height="33" /></p>
					<div class="wrap">
						<p>ご訪問・商品説明</p>
                         <p class="last">ご希望日時に専門スタッフが商品説明にお伺いします。</p>
                    </div>
                                               
                    <div class="box_pickup">
                        <p class="pickup_tit"><span>PICK   UP</span>  　　　回収プランは2つご用意しています</p>
                        <dl>
                        	<dt>予約回収</dt>
                            <dd>お客様からご連絡を受けて、スタッフが回収に伺います<br />
※ご希望の日時をお伝えください</dd>
                        </dl>
                        <dl class="box_right">
                        	<dt>おまかせ回収</dt>
                            <dd>一定周期でスタッフが回収に伺います</dd>
                        </dl>
						</div>
                        
					</div>
				<!-- / .orangeFrameBox --></div>
                
                <div class="orangeFrameBox next">
				<div class="orangeFrameInner cf">
					<p class="imgL"><img src="<?php bloginfo('template_url');?>/img/common/icon_number03.png" alt="3" width="30" height="33" /></p>
					<div class="wrap">
						<p>お見積もり・納入（導入）</p>
						<p class="asterisk last">お見積もりにご納得いただけましたら、「すて丸くん」をご指定の場所に納品します。</p>
						</div>
					</div>
				<!-- / .orangeFrameBox --></div>
                
                               
                
                 <h4><img src="<?php bloginfo('template_url');?>/img/blackbox_sti03.png" alt="回収処理の流れ" width="710" height="39" /></h4>
                
                <div class="orangeFrameBox">
				<div class="orangeFrameInner cf">

					<p class="imgL"><img src="<?php bloginfo('template_url');?>/img/common/icon_number01.png" alt="1" width="30" height="33" /></p>
					<div class="wrap">
						<p>回収処理のご連絡（※予約回収の場合のみ）</p>
						<p>回収箱がいっぱいになる前にご連絡ください。おまかせ回収でない方は、ご希望の回収日をお申し付けください。</p>
                        <p class="last"><span class="txt_org">回収のご予約</span><br />
                        【お電話】　06-6328-4800<br />
【FAX】　06-6328-4802<br />
※おまかせ回収の場合はご連絡不要です</p>
				  </div>
				  </div>
				<!-- / .orangeFrameBox --></div>
                
                <div class="orangeFrameBox next">
				<div class="orangeFrameInner cf">
					<p class="imgL"><img src="<?php bloginfo('template_url');?>/img/common/icon_number02.png" alt="2" width="30" height="33" /></p>
					<div class="wrap">
						<p>回収・運搬</p>
                         <p class="last">お客様のご希望日時に、弊社の専門スタッフが回収にお伺いします。ボックスは密閉シールを貼り未開封の状態で専用車に積み、積み替えなしで即日リサイクル（溶解）処理場へ運搬します。</p>
                    </div>
                                               

                        
					</div>
				<!-- / .orangeFrameBox --></div>
                
                <div class="orangeFrameBox next">
				<div class="orangeFrameInner cf">
					<p class="imgL"><img src="<?php bloginfo('template_url');?>/img/common/icon_number03.png" alt="3" width="30" height="33" /></p>
					<div class="wrap">
						<p>廃棄処理</p>
						<p class="last">リサイクル処理場に到着したら、未開封のままリサイクル（溶解）処理を行います。チャーター便でご予約頂いているお客様に限り、溶解状況をWebカメラでご確認頂けます。</p>
						</div>
					</div>
				<!-- / .orangeFrameBox --></div>
                
                <h5><img src="<?php bloginfo('template_url');?>/img/blackbox_cap03.png" alt="こんなシーンでご利用いただけます" width="352" height="21" /></h5>
                <table class="tb">
                <col width="33%" />
                <col width="34%" />
                <col width="33%" />
                	<tr>
                    	<td class="center"><img src="<?php bloginfo('template_url');?>/img/blackbox_img02.jpg" alt="こんなシーンでご利用いただけます" width="144" height="105" /></td>
                        <td class="center"><img src="<?php bloginfo('template_url');?>/img/blackbox_img03.jpg" alt="こんなシーンでご利用いただけます" width="144" height="105" /></td>
                        <td class="center"><img src="<?php bloginfo('template_url');?>/img/blackbox_img04.jpg" alt="こんなシーンでご利用いただけます" width="144" height="105" /></td>
                    </tr>
                    <tr>
                    	<td><p class="bold">機密文書</p>
                        	   <p>新商品の仕様書、開発企画書、特許関連書類、社外秘文書、未公開の調査票</p>
                        </td>
                        <td><p class="bold">顧客情報</p>
                        	   <p>顧客の住所、電話番号など個人を特定できる書類（アンケート、契約書など）</p></td>
                        <td><p class="bold">保管文書</p>
                        	   <p>出納帳、納品書控、請求書控、決算書類、伝票、記録文書など</p></td>
                    </tr>
                </table>
                <p class="center"><img src="<?php bloginfo('template_url');?>/img/txt_arrow.jpg" alt="「すて丸くん」の導入をご検討ください！" width="613" height="151" /></p>
               <p>個人情報の漏洩が不安な方、シュレッダー手間とコストを省きたい方はぜひ「すて丸くん」の導入をご検討ください。「すて丸くん」は、<span class="bold">廃棄書類を溶解まで未開封のまま運搬し、かつ運搬状況や溶解の様子をお客様がご確認いただける</span>ことが特長です。安全かつローコストの廃棄サービスをご提供します。</p> 
                

			
			
			<h3 id="blackboxSec"><img src="<?php bloginfo('template_url');?>/img/blackbox_ti02.png" alt="BLACKBOX" /></h3>
			<div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/blackbox_img01.jpg" alt="" /></p>
				<p>専用のカギ付きハードケース（ジュラルミン製）を利用し、施錠したままハードケースごと回収し、処理工場へ運搬するサービスです。金融機関の現金輸送に用いられている封印システムを採用しています。処理現場に到着するまで一切解錠することはありませんので、安全かつ確実な溶解処理が可能です。</p>
				<p class="last">また、すて丸くんと同様、運搬車両にはGPS（位置情報システム）を搭載。万が一の事故に備え、回収から処理場までの移動を常に監視します。</p>
			</div>
			<h4><img src="<?php bloginfo('template_url');?>/img/blackbox_sti05.png" alt="BLACKBOXの特長" /></h4>
			<p class="last">BLACKBOXにはすて丸くんの5つの特長に加え、以下のふたつの特長があります。</p>
			<h5><img src="<?php bloginfo('template_url');?>/img/blackbox_cap01.png" alt="回収箱はジュラルミン製のハードケース" /></h5>
			<p class="last">BLACKBOXはジュラルミン製の強固なハードケース。高いセキュリティー性を誇り、社内管理者による解錠がなければ中身を取り出すことは一切できません。</p>
			<h5><img src="<?php bloginfo('template_url');?>/img/blackbox_cap02.png" alt="現金輸送車と同様の封印システムを採用" /></h5>
			<p class="last">金融機関の現金輸送に用いられている封印システムを採用し、処理現場に到着するまで一切解錠することはありません。また、解錠したことが確認できるよう、封印錠の蓋を押し込む際に封紙を挟み込んでいます（解錠されると封紙が破れ、解錠したことが確認できます）。</p>
			
			
			<h4><img src="<?php bloginfo('template_url');?>/img//blackbox_sti06.png" alt="回収処理の流れ" /></h4>
			<h5 class="capBg firstChild" id="flow01">オフィスでの手順</h5>
			<div class="galleryBox cf">
				<p class="zoomWrap"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_01.jpg" width="216" height="253" class="zoomImg" alt="" /></p>
				<div class="thumbBox">
					<ul class="cf">
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_01.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb01.jpg" alt="1.BLACKBOX全景" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_02.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb02.jpg" alt="2.BLACKBOX用内袋（当社にてセット）" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_03.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb03.jpg" alt="3.BLACKBOX内に内袋セット" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_04.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb04.jpg" alt="4.封印無しで施錠後にご使用いただきます" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_05.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb05.jpg" alt="5.機密文書が満載" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_06.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb06.jpg" alt="6.機密文書がたまったらBLACKBOXをオープン" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_07.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb07.jpg" alt="7.内袋を梱包（お客様）" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_08.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb08.jpg" alt="8.投入口も内側よりクローズ" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_09.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb09.jpg" alt="9.各ロックを施錠する" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_10.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb10.jpg" alt="10.施錠の確認" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_11.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb11.jpg" alt="11.封印シールに確認印＋日付記入" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_12.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb12.jpg" alt="12.封印シールを鍵穴に差し込む" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_13.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb13.jpg" alt="13.施錠（確認印は外側向き）" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_14.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb14.jpg" alt="14.施錠完了" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow01_15.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow01_thumb15.jpg" alt="15.BLACKBOX回収→溶解工場へ" /></a></li>
					</ul>
				<!-- / .thumbBox --></div>
			<!-- / .galleryBox --></div>
			<p class="pagetop"><a href="#wrapper"><img src="<?php bloginfo('template_url');?>/img/common/pagetop_off.gif" alt="page top" /></a></p>
			
			<h5 class="capBg" id="flow02">溶解工場での手順</h5>
			<div class="galleryBox cf">
				<p class="zoomWrap"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow02_01.jpg" width="216" height="253" class="zoomImg" alt="" /></p>
				<div class="thumbBox">
					<ul class="cf">
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow02_01.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow02_thumb01.jpg" alt="A.溶解工場に到着" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow02_02.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow02_thumb02.jpg" alt="B.溶解工場責任者により開錠" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow02_03.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow02_thumb03.jpg" alt="C.封印シールを取り出す" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow02_04.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow02_thumb04.jpg" alt="D.両ドアオープン" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow02_05.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow02_thumb05.jpg" alt="E.溶解物（未開封）をコンベアーへ" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow02_06.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow02_thumb06.jpg" alt="F.コンベアーよりパルパーへ" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow02_07.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow02_thumb07.jpg" alt="G.パルパー投入口" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow02_08.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow02_thumb08.jpg" alt="H.パルパーへ投入（溶解）" /></a></li>
						<li><a href="<?php bloginfo('template_url');?>/img/blackbox_flow02_09.jpg"><img src="<?php bloginfo('template_url');?>/img/blackbox_flow02_thumb09.jpg" alt="I.パルパー内にて溶解完了" /></a></li>
					</ul>
				<!-- / .thumbBox --></div>
			<!-- / .galleryBox --></div>
			
			<p class="contentBnr last"><a href="<?php bloginfo('url');?>/about"><img src="<?php bloginfo('template_url');?>/img/common/bnr_about_off.png" alt="古紙回収・リサイクル業を続けて60年以上 西川が選ばれる5つの安心ポイントはこちら" /></a></p>
			<p class="contentBnr last"><a href="<?php bloginfo('url');?>/contact"><img src="<?php bloginfo('template_url');?>/img/common/contentbnr_fmail_off.jpg" alt="お電話・FAX・メールにてお気軽にお問い合わせ下さい！0120-107-254 06-6328-4800 FAX06-6327-1104 【受付】9:00～18:00（第1・3土曜日・日祝除く）　スピード無料見積 お申し込み受付中！" /></a></p>
<?php get_footer(); ?>