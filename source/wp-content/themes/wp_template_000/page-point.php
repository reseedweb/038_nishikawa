<?php get_header(); ?>
			<h3 class="firstChild"><img src="<?php bloginfo('template_url');?>/img/point_ti01.png" alt="古紙回収を安心して任せるために" /></h3>
			<p class="last">古紙回収は手軽に機密文書を処分できる便利なサービスですが、他者に大事な文書を預けるという意味では、漏洩のリスクなどは十分に考慮すべきところ。万が一文書の内容が他者に漏れてしまっては、企業の信頼が失墜することにもなりかねません。また、今後継続して依頼することになりますので、対応力の高い業者を選んでおくことも大切です。</p>
			<h4><img src="<?php bloginfo('template_url');?>/img/point_sti01.png" alt="その1　信頼できる業者かどうか" /></h4>
			<div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/point_img01.jpg" alt="" /></p>
				<p class="last">業者が信頼できるかどうかは、もっとも基本的かつ重要なポイント。問い合わせた際の対応やホームページの内容、ISOやPマークの取得の有無などから、信頼の置ける業者かどうかを確認してください。また、古紙回収業を同じ場所でどれくらい続けているかも、業者の信頼性を知るうえで重要な指標になります。</p>
			</div>
			<h4><img src="<?php bloginfo('template_url');?>/img/point_sti02.png" alt="その2　セキュリティー対策は万全かどうか" /></h4>
			<div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/point_img02.jpg" alt="" /></p>
				<p class="last">いくら安全を謳っていても、回収してからの流れが不透明な業者には安心して任せることはできません。回収後の回収車の動きを把握しているか、処理の現場を監視しているかなど、回収から処理までのセキュリティー対策が万全の業者を選ぶことが大切です。</p>
			</div>
			<h4><img src="<?php bloginfo('template_url');?>/img/point_sti03.png" alt="その3　対応は早いかどうか" /></h4>
			<div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/point_img03.jpg" alt="" /></p>
				<p class="last">「見積もりが出るまでに何日も待たされる」「回収の依頼をしてもすぐに来てくれない」など、業者によっては十分な作業体制が整っておらず、対応が遅いこともあります。場合によっては回収した書類が何日も回収業者のもとに放置されてしまうこともありますので、すぐに見積もりをしてくれることや、即日処理ができることなどを基準に業者を選ぶようにすると、より安心です。</p>
			</div>
			<p class="contentBnr last"><a href="<?php bloginfo('url');?>/about"><img src="<?php bloginfo('template_url');?>/img/common/bnr_about_off.png" alt="古紙回収・リサイクル業を続けて60年以上 西川が選ばれる5つの安心ポイントはこちら" /></a></p>
			<p class="contentBnr last"><a href="<?php bloginfo('url');?>/contact"><img src="<?php bloginfo('template_url');?>/img/common/contentbnr_fmail_off.jpg" alt="お電話・FAX・メールにてお気軽にお問い合わせ下さい！0120-107-254 06-6328-4800 FAX06-6327-1104 【受付】9:00～18:00（第1・3土曜日・日祝除く）　スピード無料見積 お申し込み受付中！" /></a></p>		
<?php get_footer(); ?>