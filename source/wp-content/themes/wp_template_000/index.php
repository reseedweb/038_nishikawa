<?php get_header(); ?>
	<p class="contentBnr2"><a href="<?php bloginfo('url'); ?>/contact"><img src="<?php bloginfo('template_url'); ?>/img/index_btn_campain_off.jpg" alt="初回50%オフキャンペーン実施50箱以上お見積りのお客様を月に3社まで" width="710" height="230" border="0" /></a></p>
	<p class="mt10"><img src="<?php bloginfo('template_url'); ?>/img/index_hyo.jpg" alt="是非比べてみてください！この価格　他社比較表" width="710" height="255" border="0" /></p>
	<p><img alt="top" src="<?php bloginfo('template_url'); ?>/img/banner_index.jpg" width="710" height="289" border="0" /></p>
	<p class="textLinkb"><a href="http://www.city.osaka.lg.jp/kankyo/page/0000171295.html" target="_blank">→ → → 詳しくは、大阪市のホームページへ</a></p>
	<p class="contentBnr"><img src="<?php bloginfo('template_url'); ?>/img/index_img04.jpg" alt="初回50%オフキャンペーン実施一ヶ月3社まで限定" width="710" height="130" border="0" /></p>
	<p><img src="<?php bloginfo('template_url'); ?>/img/index_img05.jpg" alt="0120-107-254" width="420" height="55" border="0" /><a href="<?php bloginfo('url'); ?>/contact"><img src="<?php bloginfo('template_url'); ?>/img/index_btn_mail_off.jpg" alt="メールはこちらから" width="255" height="55" border="0" /></a></p>
	<p class="contentBnr"><img src="<?php bloginfo('template_url'); ?>/img/index_img02.jpg" alt="西川がここまでお安くサービスできるのも理由がある！" width="710" height="470" border="0" /></p>
	
	<h3 class="firstChild"><img src="<?php bloginfo('template_url'); ?>/img/index_ti01.png" alt="What's New &amp; Topics" /></h3>
	<?php get_template_part('part','blog');?>	
	
	<div class="tiBox cf">
		<p class="fleft last"><a href="about/"><img src="<?php bloginfo('template_url'); ?>/img/index_bnr_about_off.png" alt="安全かつ確実に処理する当社の5つの安心ポイント　1.即お見積りのスピード対応 2.安心のGPSでの機密文書処理 3.創業約60年以上の対応 4.溶解処理証明書の発行 5.補償制度完備" /></a></p>
		<ul class="itemList fright">
			<li><a href="service/blackbox.html#blackboxSec"><img src="<?php bloginfo('template_url'); ?>/img/index_bnr_blackbox_off.jpg" alt="機密文書の漏洩を阻止！GPSで最終処理まで追跡管理！" /></a></li>
			<li class="last"><a href="service/blackbox.html#sutemarukunSec"><img src="<?php bloginfo('template_url'); ?>/img/index_bnr_sutemarukun_off.jpg" alt="すて丸くん 1箱からご対応！ 重要文書を未開封のまま、その日の内にリサイクル処理！" /></a></li>
	  </ul>
	</div>
	
	<h3><img src="<?php bloginfo('template_url'); ?>/img/index_ti03.png" alt="初めてご依頼の方へ！賢く使う古紙回収業者！" /></h3>
	<ul id="bgCheck" class="cf">
		<li class="fleft"><a href="benefits.html"><img src="<?php bloginfo('template_url'); ?>/img/index_bnr_benefits_off.png" alt="頼む前に知っておきたい！古紙･機密文書処理について" /></a></li>
		<li class="fright"><a href="point.html"><img src="<?php bloginfo('template_url'); ?>/img/index_bnr_point_off.png" alt="これを知れば大丈夫！失敗しない業者選びの3つのポイント" /></a></li>
	</ul>
	
	<h3><img src="<?php bloginfo('template_url'); ?>/img/index_ti04.png" alt="安心の古紙回収・処理なら株式会社西川へ" /></h3>
	<div class="cf">
		<p class="imgR"><img src="<?php bloginfo('template_url'); ?>/img/index_img01.jpg" alt="" /></p>
		<p>古紙・機密文書の回収や溶解・廃棄処理を行う株式会社西川のWebサイトへお越しいただき、誠にありがとうございます。当社は大阪市で60年以上、古紙・機密文書の溶解・廃棄・回収業に携わってまいりました。おかげさまで現在では、法人のお客様だけで全国で2,000社を超える企業様とお取り引きさせていただき、日々、お客様における機密文書といった大切な書類を安全に溶解・廃棄・処理するべく、社員一丸となって取り組んでおります。</p>
		<p>当社の強みとしては、古紙・機密文書の溶解や廃棄、回収からリサイクル事業まで特化しており、お客様の大事な情報媒体である機密文書の回収や溶解・廃棄処理を一貫サポートしております。<br />
			具体的に回収した機密文書・古紙を確実にドライバーが運搬するため、運搬車両にGPSを搭載し、回収から処理場までの移動を常時監視しております。<br />
			また、お預かりした機密文書を溶解・廃棄処分したことを証明する上で、溶解処理証明書を発行しています。<br />
			溶解・廃棄処理に関しましては、長年の提携先製紙工場にて行っておりますので、安心してご依頼お任せください。</p>
		<p>当社では関西の古紙・機密文書の溶解・廃棄・回収業者としてはじめてのPマーク取得業者になるなど、常に機密文書にまつわる情報管理のパイオニアとして業界をリードし、古紙・機密文書の安全な溶解・廃棄・回収・処理を行ってきました。不要な機密文書の書類廃棄処分にお悩みの方、シュレッダー作業など廃棄にかかる時間・コスト削減をお考えの方は、どうぞ当社までご相談ください。</p>
	</div>
    <div class="tiBox cf">
		<div class="greenFrameBox contentNavBox">
			<ul class="greenFrameInner">
				<li><a href="about/flow.html"><img src="<?php bloginfo('template_url'); ?>/img/index_btn_flow_off.png" alt="回収までの流れ" /></a></li>
				<li><a href="about/faq.html"><img src="<?php bloginfo('template_url'); ?>/img/index_btn_faq_off.png" alt="よくあるご質問" /></a></li>
				<li class="last"><a href="about/company.html"><img src="<?php bloginfo('template_url'); ?>/img/index_btn_company_off.png" alt="会社概要" /></a></li>
			</ul>
		</div>
		<div class="wrap">
			<h4 class="firstChild"><img src="<?php bloginfo('template_url'); ?>/img/index_sti01.png" alt="環境保護を推進しています" /></h4>
			<p>当社では古紙・機密文書の廃棄処理方法に溶解処理を選ぶことで、回収した機密文書等の紙を廃棄処理した上でリサイクルしています。また、使用済みのトナーカートリッジやインクボトルの回収・リサイクル、リサイクルコピー機のレンタルなど、各種OAサプライのリサイクル事業も展開しています。</p>
			<p>当社のモットーは「リサイクルを通して社会に貢献する」こと。お客様にお求めやすい価格でのサービスをご提供することはもちろん、環境保護を推進していくことで、皆様にとってより住みよい社会をつくっていければと願っています。</p>
		</div>
	</div>
	<p class="contentBnr last"><a href="fmail/fmail.cgi" onClick="_gaq.push(['_trackEvent', 'mail', 'fmail.cgi','top_under']);"><img src="<?php bloginfo('template_url'); ?>/img/common/contentbnr_fmail_off.jpg" alt="お電話・FAX・メールにてお気軽にお問い合わせ下さい！0120-107-254 06-6328-4800 FAX06-6327-1104 【受付】9:00～18:00（第1・3土曜日・日祝除く）　スピード無料見積 お申し込み受付中！" /></a></p>					
<?php get_footer(); ?>