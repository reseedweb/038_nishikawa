<?php get_header(); ?>
<h3 class="firstChild"><img src="<?php bloginfo('template_url'); ?>/img/faq_ti01.png" alt="古紙回収・カートリッジリサイクルQ＆A" /></h3>
			<p class="last">株式会社西川によくお寄せいただくご質問をQ＆A形式でご紹介します。こちらに掲載していない内容のご質問につきましては、お気軽にお問い合わせください。</p>
			<ul class="tiBox anchorList cf">
				<li><a href="#faq01"><img src="<?php bloginfo('template_url'); ?>/img/faq_anchor_q01_off.png" alt="古紙回収について" /></a></li>
				<li><a href="#faq02"><img src="<?php bloginfo('template_url'); ?>/img/faq_anchor_q02_off.png" alt="カートリッジリサイクルについて" /></a></li>
			</ul>
			
			<h4 id="faq01"><img src="<?php bloginfo('template_url'); ?>/img/faq_sti01.png" alt="古紙回収について" /></h4>
			<h5 class="questionTi firstChild">古紙は分別しなくても回収してもらえますか？</h5>
			<div class="answerBox">
				<p class="last">はい、分別なしでも回収を承っています。ただし、未開封での処理をご希望の場合は、事前に分別していただく必要があります。なお、未分別で回収した古紙につきましては、PマークやISO14001に関する教育を受けた専門スタッフが処理の種類ごとに選別させていただきますので、安心してお任せください。</p>
			</div>
			<h5 class="questionTi">機密保持契約は結んでいただけますか？</h5>
			<div class="answerBox">
				<p class="last">はい、もちろんです。当社でご提供しているすべての古紙回収サービスにおいて、ご契約時に機密保持契約を交わしています。</p>
			</div>
            
            <h5 class="questionTi">書類はどのようにして回収しているのですか？</h5>
			<div class="answerBox">
				<p class="last">近畿圏は回収にお伺い致します。(回収量が少ない場合は宅配便でのご対応になる可能性もございます)近畿圏外は宅配便にてご対応させて頂きます。</p>
			</div>
            
            <h5 class="questionTi">大阪府下ではないのですが本当に取りに来てもらえるのですか？</h5>
			<div class="answerBox">
				<p>量が少ない場合はルート回収の途中でお立ち寄りさせて頂く対応となります。どのくらいの量があるのか御社の地域はルート回収のタイミングがいつなのか一度お問い合わせ下さい。タイミングが合わない場合には宅配便での回収もご好評頂いているので合わせてご検討下さい。</p>
                <p class="last"><a href="<?php bloginfo('url'); ?>/service#service03">DS Pack Systemsについて詳しくはこちら</a></p>
			</div>

			<h5 class="questionTi">遠方でも対応していただけますか？</h5>
			<div class="answerBox">
				<p class="last">はい。日本全国を対象に、宅配便による古紙回収サービスをご提供しています。カギ付きのハードケースをご利用いただき、ケース内の中箱を未開封のまま溶解処理するサービスですので、安心してご利用いただけます。</p>
			</div>
			<h5 class="questionTi">少量の回収にも対応していますか？</h5>
			<div class="answerBox">
				<p class="last">当社では、「DS Pack Systems」という1箱から機密文書を回収させていただくサービスをご提供しています。当社指定の回収用ボックスをご利用いただくとても手軽なサービスですので、ぜひご検討ください。</p>
				<p class="txtLink last"><a href="<?php bloginfo('url'); ?>/service#service03">DS Pack Systemsについて詳しくはこちら</a></p>
			</div>
			<h5 class="questionTi">個人での利用も可能ですか？</h5>
			<div class="answerBox">
				<p class="last">はい、もちろんです。個人のお客様からのご依頼も承っていますので、お気軽にお問い合わせください。</p>
				<p class="txtLink last"><a href="<?php bloginfo('url'); ?>/flow">ご依頼からの流れはこちら</a></p>
			</div>
			<h5 class="questionTi">溶解処理の現場に立ち会うことはできますか？</h5>
			<div class="answerBox">
				<p class="last">はい、ご希望に応じて溶解処理の立ち会いを承っています。また、溶解処理の様子をWebカメラを通じてご覧いただくこともできますので、お気軽にお申し付けください。</p>
			</div>			
			<h4 id="faq02"><img src="<?php bloginfo('template_url'); ?>/img/faq_sti02.png" alt="カートリッジリサイクルについて" /></h4>
			<h5 class="questionTi firstChild">カートリッジリサイクルとはどういったものですか？</h5>
			<div class="answerBox">
				<p class="last">お客様から回収した使用済トナーカートリッジを分解、洗浄、修理、部分交換し、純正品と同量の新しいトナーを充填し、再利用するサービスです。</p>
			</div>
			<h5 class="questionTi">どんなカートリッジに対応していますか？</h5>
			<div class="answerBox">
				<p class="last">レーザープリンターやインクジェットプリンター、FAXのカートリッジや、軽印刷機用のリサイクルインクボトルに対応しています。各種機器の対応状況など、詳しくはお気軽にお問い合わせください。</p>
			</div>
			<h5 class="questionTi">品質に問題はありませんか？</h5>
			<div class="answerBox">
				<p class="last">当社では独自開発の高品質カートリッジをご提供し、各カートリッジのリサイクル回数や交換部品などを記録しています。リサイクル時にはカルテの記録に基づいた再生加工処理を施し、厳重な印刷テストのうえでお客様にお届けしています。品質に問題はありませんので、安心してご利用いただけます。</p>
			</div>
			
			<p class="contentBnr last"><a href="<?php bloginfo('url'); ?>/contact"><img src="<?php bloginfo('template_url'); ?>/img/common/contentbnr_fmail_off.jpg" alt="お電話・FAX・メールにてお気軽にお問い合わせ下さい！0120-107-254 06-6328-4800 FAX06-6327-1104 【受付】9:00～18:00（第1・3土曜日・日祝除く）　スピード無料見積 お申し込み受付中！" /></a></p>
<?php get_footer(); ?>