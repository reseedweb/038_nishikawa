<?php get_header(); ?>
	<h3 class="privacy-title"><img src="<?php bloginfo('template_url'); ?>/img/company_ti03.png" alt="個人情報の取り扱いについて" /></h3>
	<h4 class="capBg"><span>1.当社の名称</span></h4>
	<p class="last">株式会社　西川</p>
	<h4 class="capBg"><span>2.個人情報保護管理者および個人情報問い合わせ窓口</span></h4>
	<p class="last">顧問　野島清<br />
		大阪府大阪市東淀川区大隅1丁目5番23号<br />
	    TEL：06-6328-4800<br />
		FAX：06-6328-4802<br />
		E-MAIL：<a href="mailto:privacy@nishikawa.com">privacy@nishikawa.com</a></p>
	<h4 class="capBg"><span>3.個人情報の利用目的の公表</span></h4>
	<p>当社が、書面で本人より直接取得する場合および間接的に取得する場合の個人情報、ならびに個人データの利用目的は、以下の通りです。</p>
	<h5>（1）従業員情報</h5>
	<div class="numSection">
		<p class="last">当社従業員の雇用・人事管理上必要なため、ならびに募集採用・退職者からの問い合わせのため</p>
	</div>
	<h5>（2）お客様からお預かりした機密書類</h5>
	<div class="numSection">
		<p class="last">溶解リサイクルのため<br />
			※開示対象につきましては（1）のみとなります</p>
	</div>
	<h4 class="capBg"><span>4.個人情報の開示および訂正・追加または削除についての手続き</span></h4>
	<p class="last">当社では、本人または代理人からの開示の求め、および訂正・追加または削除の求めに対応させていただいております。</p>
	<h5>（1）開示等の求めの申出先</h5>
	<div class="numSection">
		<p>開示等の求めについては、2に記載する窓口にお電話をいただければ、10営業日以内に対応いたします。開示の結果、誤った情報があり、訂正または削除が必要であれば、速やかに対応し、本人にその旨を通知いたします。</p>
	</div>
	<h5>（2）開示等の求めに際しての本人確認の方法</h5>
	<div class="numSection">
		<p class="last">来社の場合は、運転免許証、健康保険被保険証のいずれか現住所が記載されているものをご提示いただき、本人の確認をいたします。また、個人情報開示申請書をご記入・ご提出いただきます。お電話の場合は、本人の氏名・住所・生年月日をお伝えいただきます。</p>
	</div>
	<h5>（3）代理人による開示等の求め</h5>
	<div class="numSection">
		<p class="last">開示等の求めをする者が未成年者または成年被後見人の法定代理人もしくは開示等の求めをすることにつき本人が委任した代理人である場合は、委任状をご提出いただきます。</p>
	</div>
	<h5>（4）手続きに要する費用について</h5>
	<div class="numSection">
		<p class="last">郵送の場合は、郵便・宅配にかかる実費費用をいただきます。また、「利用目的の通知」「開示」を請求される場合は、手数料として1,000円をいただきます。</p>
	</div>
		<h5>（5）認定個人情報保護団体の名称および苦情の解決の申し出先</h5>
	<div class="numSection">
		<p class="last">当社は次の認定個人情報保護団体の対象事業者となっています。<br />
		名称：<a href="http://privacymark.jp/protection_group/" target="_blank">一般財団法人　日本情報経済社会推進協会</a></p>
	</div>
<?php get_footer(); ?>