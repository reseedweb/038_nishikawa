<?php get_header(); ?>
			<h3 class="firstChild"><img src="<?php bloginfo('template_url');?>/img/recruit_ti01.png" alt="採用について" /></h3>
			<p class="last">株式会社西川では古紙回収業やOAサプライのリサイクル業を通して、お客様の手間や時間・コストの削減はもとより、地球の環境保全に貢献できる企業を目指しています。当社では、こうした当社の取り組みに共感し、やる気と行動力を持ち、共に当社のこれからをつくってくれる力を募集しています。</p>
			<h4 class="capBg"><span>中途採用</span></h4>
			<p>現在、募集はしておりません。ご了承ください。</p>
			<p class="contentBnr last"><a href="<?php bloginfo('url');?>/contact"><img src="<?php bloginfo('template_url');?>/img/common/contentbnr_fmail_off.jpg" alt="お電話・FAX・メールにてお気軽にお問い合わせ下さい！0120-107-254 06-6328-4800 FAX06-6327-1104 【受付】9:00～18:00（第1・3土曜日・日祝除く）　スピード無料見積 お申し込み受付中！" /></a></p>	
<?php get_footer(); ?>