<?php get_header(); ?>
			<h3 class="firstChild"><img src="<?php bloginfo('template_url');?>/img/oa_ti01.png" alt="カートリッジリサイクル" /></h3>
			<p><img src="<?php bloginfo('template_url');?>/img/oa_catchcopy01.gif" alt="資源の節約で御社のコストを大幅削減！！" /></p>
			<div class="cf tiBox">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/oa_img01.jpg" alt="" /></p>
				<p class="last">株式会社西川では、使用済みのトナーカートリッジやインクボトルを回収し、トナー・インクの充填を行うカートリッジリサイクルサービスをご提供しています。容器を捨てることなく再利用できるため資源の節約につながるとともに、御社のコストも大幅に削減可能。リサイクルカートリッジの販売も行っていますので、お気軽にご相談ください。</p>
			</div>
			
			<h4><img src="<?php bloginfo('template_url');?>/img/oa_sti04.png" alt="地球環境保全に貢献" /></h4>
			<div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img//oa_img07.jpg" alt="" /></p>
				<p class="last">使用済みのトナーカートリッジは産業廃棄物に分類されます。トナーカートリッジを再利用することで産業廃棄物の削減につながります。地球の環境汚染を阻止するための身近な環境活動です。</p>
			</div>
			
			<h4><img src="<?php bloginfo('template_url');?>/img/oa_sti05.png" alt="経費を大幅に削減" /></h4>
			<div class="cf">
				<p class="imgL"><img src="<?php bloginfo('template_url');?>/img/oa_img08.jpg" alt="" /></p>
				<p class="last">産業廃棄物に分類されるトナーカートリッジは処分にも費用がかかります。トナーカートリッジを再利用することで、廃棄して新品を購入する場合と比べて約30～80％の経費削減が見込めます。</p>
			</div>
			
			<h4><img src="<?php bloginfo('template_url');?>/img/oa_sti01.png" alt="カートリッジリサイクルメニュー" /></h4>
			<h5 class="capBg firstChild"><span>レーザープリンター＆FAX用トナーカートリッジ</span></h5>
			<div class="cf">				
				<p class="last">当社独自のノウハウを用いた高品質のリサイクルカートリッジを低価格にてご提供しています。各カートリッジはカルテで管理され、リサイクル回数や交換部品などを記録。リサイクル時にはカルテの記録に基づいた再生加工処理を施し、厳重な印刷テストの後、お客様にお届けしています。即日納品も可能です。</p>
			</div>
			<h5 class="capBg"><span>インクジェットプリンター用リペアカートリッジ</span></h5>
			<div class="cf">				
				<p class="last">使用済みのカートリッジを洗浄し、インクを充填します。色の濁りやくすみを起こさないように、厳しい管理体制のもと純正品と同様の品質再現を行っています。</p>
			</div>
			<h5 class="capBg"><span>軽印刷機用リサイクルインクボトル</span></h5>
			<div class="cf">				
				<p class="last">お客様が使用されたインク容器を回収し、独自の技術でリサイクル加工を行うことで、品質を維持したままインクを充填しています。</p>
			</div>
			
			<h4><img src="<?php bloginfo('template_url');?>/img/oa_sti02.png" alt="カートリッジリサイクルの流れ" /></h4>
			<div id="oaFlowBox">
				<ol>
					<li id="oaFlow01_01"><img src="<?php bloginfo('template_url');?>/img/oa_flow01_01.png"  alt="リターン現品リサイクル" /></li>
					<li id="oaFlow01_02"><img src="<?php bloginfo('template_url');?>/img/oa_flow01_02.png"  alt="プール品（リサイクル済在庫品）・新品（ノーブランド）" /></li>
					<li id="oaFlow02"><img src="<?php bloginfo('template_url');?>/img/oa_flow02.png"  alt="ご注文 ご注文は専用発注書によるFAXにて受け付けます。" /></li>
					<li id="oaFlow03_01"><img src="<?php bloginfo('template_url');?>/img/oa_flow03_01.png"  alt="回収 運送業者が空カートリッジを回収に伺います。" /></li>
					<li id="oaFlow03_02"><img src="<?php bloginfo('template_url');?>/img/oa_flow03_02.png"  alt="在庫確認 在庫を確認します。在庫がない場合はご連絡します。" /></li>
					<li id="oaFlow04_01"><img src="<?php bloginfo('template_url');?>/img/oa_flow04_01.png"  alt="回収 使用済みカートリッジの回収も同時にご発注いただいた場合は、運送業者が回収に伺います。" /></li>
					<li id="oaFlow04_02"><img src="<?php bloginfo('template_url');?>/img/oa_flow04_02.png"  alt="入庫 入庫したカートリッジはロット番号により管理され、リサイクル前のテストプリント結果に基づきカルテが作成されます。" /></li>
					<li id="oaFlow05_01"><img src="<?php bloginfo('template_url');?>/img/oa_flow05_01.png"  alt="再生加工 カルテの内容に基づき、再生加工処理を施します。清掃、分解、トナー充填を行った後、テストプリントによる最終チェックを行います。" /></li>
					<li id="oaFlow06"><img src="<?php bloginfo('template_url');?>/img/oa_flow06.png"  alt="出荷 厳重な梱包のうえ発送します。原則として出荷日翌日に納品予定ですが（全国）、離島などの場合は配送が遅れる場合があります。" /></li>
					</ol>
			</div>
			
			
			<h3><img src="<?php bloginfo('template_url');?>/img/oa_ti02.png" alt="リサイクルコピー機レンタル" /></h3>
			<div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/oa_img05.jpg" alt="" /></p>
				<p class="last">当社ではリサイクルコピー機（オーバーホール機）のレンタルを行っています。コピー機といえばオフィスの必需品ですが、導入にはリース料やカウンター料、トナー代などがかかるためかなりの経費がかさむもの。しかし当社のリサイクルコピー機レンタルにかかる費用はカウンター料のみ。コピー機本体のリース料やトナー代、メンテナンス料不要で、新品同様の性能のコピー機をレンタルしています。</p>
			</div>
			<h4><img src="<?php bloginfo('template_url');?>/img/oa_sti03.png" alt="リサイクルコピー機レンタルのメリット" /></h4>
			<p class="last center"><img src="<?php bloginfo('template_url');?>/img/oa_merit01.png" width="710" height="220" alt="リースで新品を導入するよりもコストを大幅に削減できます・当社の独自技術を駆使し、丁寧にオーバーホールしたコピー機をレンタルします・やむを得ない場合の途中解約に伴うリスクもほとんどありません・万が一、故障が繰り返される場合はコピー機のお取り換えが可能です・アフターサービス・メンテナンスも当社のスタッフが責任を持って対応します" /></p>
			
			
			<h3><img src="<?php bloginfo('template_url');?>/img/oa_ti03.png" alt="リサイクル製品販売" /></h3>
			<div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/oa_img06.jpg" alt="" /></p>
				<p>当社では、溶解処理後に製造されるトイレットペーパーの販売を行っています。トイレットペーパーの納品は古紙回収時に行いますので、運搬費が軽減され安価にご提供可能。「お客様から回収した古紙でつくられたトイレットペーパーをお客様が使う」という、完全な古紙の循環リサイクルが可能になります。</p>
				<p class="last">もちろん、トイレットペーパーの原料は古紙100％。通常の芯ありタイプの他、芯がなくごみを減らせる「芯なしタイプ」や交換の手間が減る「業務用ロングタイプ」をご用意しています。</p>
			</div>
			
			<p class="contentBnr last"><a href="<?php bloginfo('url');?>/about"><img src="<?php bloginfo('template_url');?>/img/common/bnr_about_off.png" alt="古紙回収・リサイクル業を続けて60年以上 西川が選ばれる5つの安心ポイントはこちら" /></a></p>			
			<p class="contentBnr last"><a href="<?php bloginfo('url');?>/contact"><img src="<?php bloginfo('template_url');?>/img/common/contentbnr_fmail_off.jpg" alt="お電話・FAX・メールにてお気軽にお問い合わせ下さい！0120-107-254 06-6328-4800 FAX06-6327-1104 【受付】9:00～18:00（第1・3土曜日・日祝除く）　スピード無料見積 お申し込み受付中！" /></a></p>	
<?php get_footer(); ?>