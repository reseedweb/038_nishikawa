				<nav id="top-navi">
					<div class="wrapper clearfix">					
						<ul id="globalNav">
							<li><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/img/common/header_gnav_home_off.png" alt="トップページ" /></a></li>
							<li><li><a href="<?php bloginfo('url'); ?>/about"><img src="<?php bloginfo('template_url'); ?>/img/common/header_gnav_about_off.png" alt="西川の5つの安心ポイント" /></a></li>
							<li><a href="<?php bloginfo('url'); ?>/flow"><img src="<?php bloginfo('template_url'); ?>/img/common/header_gnav_flow_off.png" alt="回収までの流れ" /></a></li>
							<li><a href="<?php bloginfo('url'); ?>/faq"><img src="<?php bloginfo('template_url'); ?>/img/common/header_gnav_faq_off.png" alt="よくあるご質問" /></a></li>
							<li><a href="<?php bloginfo('url'); ?>/message"><img src="<?php bloginfo('template_url'); ?>/img/common/header_gnav_message_off.png" alt="企業理念・代表挨拶" /></a></li>
							<li><a href="<?php bloginfo('url'); ?>/company"><img src="<?php bloginfo('template_url'); ?>/img/common/header_gnav_company_off.png" alt="会社概要" /></a></li>
						</ul>	
					</div>
				</nav>