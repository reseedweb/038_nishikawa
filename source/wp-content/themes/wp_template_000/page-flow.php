<?php get_header(); ?>
		<h3 class="firstChild"><img src="<?php bloginfo('template_url'); ?>/img/flow_ti01.png" alt="ご依頼からの流れ" /></h3>
			<p class="last">古紙回収をご依頼いただいてからの流れをご紹介します。株式会社西川では迅速なお見積もりを徹底しています。できる限りお客様をお待たせすることなく、最適なサービスをご提案させていただきますので、まずはお気軽にお問い合わせください。</p>
			<p class="contentBnr last"><a href="<?php bloginfo('url'); ?>/contact"><img src="<?php bloginfo('template_url'); ?>/img/common/contentbnr_fmail_off.jpg" alt="お電話・FAX・メールにてお気軽にお問い合わせ下さい！0120-107-254 06-6328-4800 FAX06-6327-1104 【受付】9:00～18:00（第1・3土曜日・日祝除く）　スピード無料見積 お申し込み受付中！" /></a></p>
			<div class="faqBox tiBox">
				<h4><span>お問い合わせ・ご依頼</span></h4>
				<div class="detailBox">
					<p class="last">お電話もしくはメールフォームよりお気軽にお問い合わせください。</p>
					</div>
			</div>
			<p class="arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/arrow_green.png" alt="" /></p>
			
			<div class="faqBox">
				<h4><span>お打ち合わせ・お見積もり</span></h4>
				<div class="detailBox">
					<p class="last">回収方法やご契約方法についてお打ち合わせを行います（現場の下見をさせていただく場合もございます）。お打ち合わせ内容をもとにお見積もりを作成し、詳しくご説明いたします。お見積もり内容にご納得いただけましたらご契約となります。</p>
					</div>
			</div>
			<p class="arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/arrow_green.png" alt="" /></p>
			
			<div class="faqBox">
				<h4><span>回収</span></h4>
				<div class="detailBox">
					<p class="last">お打ち合わせ内容に応じて回収に伺います。</p>
					</div>
			</div>
			<p class="arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/arrow_green.png" alt="" /></p>
			
			<div class="faqBox">
				<h4><span>溶解処理・リサイクル</span></h4>
				<div class="detailBox">
					<p class="last">回収した古紙は製紙工場で溶解処理され、トイレットペーパーとしてリサイクルされます。</p>
					</div>
			</div>
<?php get_footer(); ?>