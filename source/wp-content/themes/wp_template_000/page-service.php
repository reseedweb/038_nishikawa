<?php get_header(); ?>
	<h3 class="firstChild"><img src="<?php bloginfo('template_url');?>/img/service_ti01.png" alt="西川の溶解処理のご案内" /></h3>
	<p><img src="<?php bloginfo('template_url');?>/img/service_catchcopy01.gif" alt="オフィスで出た紙どのように処理していますか？西川ならコストを約90％カットできます！" /></p>
	<div class="cf tiBox">
		<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_img01.jpg" alt="" /></p>
		<p class="last">万全の機密性を誇るサービス。お客様より回収した機密文書を密封したまま、開梱することなく製紙工場で溶解・廃棄処理するため、人目に触れることは一切ありません。ご要望に応じて処理現場にお立ち会いいただくことも、Webカメラを用いてインターネット上で溶解・廃棄処理工程をご確認いただくことも可能です。</p>
	</div>
	
	<div class="cf tiBox">
		<p class="imgL"><img src="<?php bloginfo('template_url');?>/img/service_img02.jpg" alt="" /></p>
		<p class="last">そこで株式会社西川がご提案しているのが「溶解処理」です。情報漏洩によるリスクの高い重要書類や機密文書を当社がお客様から回収し、機密性を保持したまま製紙会社にて溶解処理。シュレッダー処理と比較すると時間や人件費などのコストを約90％もカットできます。また、焼却しないためCO2の発生を防ぐことができ、溶解処理された古紙は再生紙として再利用できるためとてもエコなのが特長です。</p>
	</div>
	<p class="next last">なお、以下すべてのサービスにおいて機密保持契約を交わしてのご契約となりますので、安心してお任せいただけます。</p>
	
	<ul class="tiBox anchorList cf">
		<li><a href="#service01"><img src="<?php bloginfo('template_url');?>/img/service_anchor01_off.png" alt="PDS（パーフェクトデータセキュリティー）" /></a></li>
		<li><a href="#service02"><img src="<?php bloginfo('template_url');?>/img/service_anchor02_off.png" alt="NDS（ノーマルデータセキュリティー）" /></a></li>
		<li><a href="#service03"><img src="<?php bloginfo('template_url');?>/img/service_anchor03_off.png" alt="DS Pack Systems（ディーエスパックシステム）" /></a></li>
		<li><a href="#service04"><img src="<?php bloginfo('template_url');?>/img/service_anchor04_off.png" alt="Eco Police Ban（エコポリスバン）" /></a></li>
	</ul>
	
	<h4 id="service01"><img src="<?php bloginfo('template_url');?>/img/service_sti01.png" alt="PDS（パーフェクトデータセキュリティー）" /></h4>
	<div class="cf">
		<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_img03.jpg" alt="" /></p>
		<p class="last">万全の機密性を誇るサービス。お客様より回収した機密文書を密封したまま、開梱することなく製紙工場で溶解処理するため、人目に触れることは一切ありません。ご要望に応じて処理現場にお立ち会いいただくことも、Webカメラを用いてインターネット上で処理工程をご確認いただくことも可能です。</p>
	</div>
	
	<h5 class="capBg"><span>PDSのリサイクルシステム</span></h5>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number01.png" alt="1" /></p>
				<div class="wrap">
					<p class="last">専門スタッフが機密文書をトラック1車単位で回収</p>
				</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number02.png" alt="2" /></p>
				<div class="wrap cf">
					<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_flow_pds01.jpg" alt="" /></p>
					<p class="last">リサイクル溶解処理工場へ直行</p>
				</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number03.png" alt="3" /></p>
			<div class="wrap cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_flow_pds02.jpg" alt="" /></p>
				<p class="last">文書保存箱を未開封のまま溶解・廃棄処理 </p>
				<ul class="asterisk">
					<li><span class="last">※ご要望に応じてお客様のお立ち会いや事前のご予約にてインターネット配信が可能</span></li>
				</ul>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number04.png" alt="4" /></p>
			<div class="wrap cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_flow_pds03.jpg" alt="" /></p>
				<p class="last">古紙パルプへと再生処理</p>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
		<div class="orangeFrameBox">
			<div class="orangeFrameInner cf">
				<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number05.png" alt="5" /></p>
				<div class="wrap cf">
					<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_flow_pds04.jpg" alt="" /></p>
					<p class="last">トイレットペーパーとして加工し、リサイクル</p>
				</div>
			</div>
	</div><!-- / .orangeFrameBox -->
	
	<h5 class="capBg"><span>機密文書の受け入れ基準</span></h5>
	<p class="last">すべて手で破砕でき水に溶解する紙類であること、分別済みであることを前提とさせていただきます。</p>
	<table cellspacing="0" cellpadding="0" class="type01 next">
		<col width="50%" />
		<col width="50%" />
		<tr>
			<th>紙の種類</th>
			<th>受け入れ</th>
		</tr>
		<tr>
			<td>コピー用紙（上質紙・再生紙）</td>
			<td>OK</td>
		</tr>
		<tr>
			<td>フォーム用紙（上質紙・再生紙）</td>
			<td>OK</td>
		</tr>
		<tr>
			<td>帳票類（上質紙・再生紙）</td>
			<td>OK</td>
		</tr>
		<tr>
			<td>ノンカーボン紙（感圧記録紙）</td>
			<td>多少OK</td>
		</tr>
		<tr>
			<td>紙ファイル</td>
			<td>多少OK</td>
		</tr>
		<tr>
			<td>裏カーボン紙（カーボン伝票）</td>
			<td>多少OK</td>
		</tr>
		<tr>
			<td>感圧記録紙（FAX・ワープロ用紙など）</td>
			<td>多少OK</td>
		</tr>
		<tr>
			<td>綴じ紐</td>
			<td>多少OK</td>
		</tr>
		<tr>
			<td>茶封筒・窓付き封筒</td>
			<td>多少OK</td>
		</tr>
	</table>
	
	<ul class="asterisk info">
		<li>※文書を綴じてあるクリップ・ホッチキスの針など、小さな金属は取り外し不要です</li>
		<li>※ダンボールは、クラフトテープ（紙製のもの）で梱包してください</li>
	</ul>
				
	<h4 id="service02"><img src="<?php bloginfo('template_url');?>/img/service_sti02.png" alt="NDS（ノーマルデータセキュリティー）" /></h4>
	<div class="cf">
		<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_img04.jpg" alt="" /></p>
		<p class="last">機密保持とリサイクルを可能にするPDSよりも割安なサービス。お客様から回収したオフィス古紙を、PマークやISO14001に関する教育を受けた専門スタッフが処理の種類ごとに選別してプレス加工を行い、製紙工場にて溶解・廃棄処理を行います。小ロットにも対応し、上質紙の場合は処理費用0円、分別済みの場合は割安でお見積もりします。</p>
	</div>
	
	<h5 class="capBg"><span>NDSの特長</span></h5>
		<p class="last"><img src="<?php bloginfo('template_url');?>/img/service_feature01.png" width="710" height="220" alt="・無選別で回収可能・社員による直接回収・PマークおよびISO14001の教育を受けた専門スタッフによる選別・セントラル警備保障管理下での保管・製紙原料を製紙会社へ直送" /></p>
		
	<h5 class="capBg"><span>NDSのリサイクルシステム</span></h5>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number01.png" alt="1" /></p>
			<div class="wrap">
				<p class="last">専門スタッフが機密文書をトラックで回収（小口回収可）</p>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number02.png" alt="2" /></p>
			<div class="wrap cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_flow_nds01.jpg" alt="" /></p>
				<p class="last">当社工場へ直行</p>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number03.png" alt="3" /></p>
			<div class="wrap cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_flow_nds02.jpg" alt="" /></p>
				<p class="last">Pマーク・ISO14001の教育を受けた専門スタッフによる選別</p>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number04.png" alt="4" /></p>
			<div class="wrap cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_flow_nds04.jpg" alt="" width="185" height="135" /></p>
				<p class="last">セントラル警備保障による社内監視システム下で保管</p>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number05.png" alt="5" /></p>
			<div class="wrap cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_flow_nds03.jpg" alt="" width="150" height="149" /></p>
				<p class="last">プレス加工後、製紙原料に</p>
				<ul class="asterisk">
					<li>※選別に出た溶解処理不可のごみは破砕圧縮処理後埋め立てします</li>
				</ul>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number06.png" alt="6" /></p>
			<div class="wrap cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_flow_pds01.jpg" alt="" /></p>
				<p class="last">製紙工場へ直送し溶解処理</p>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
		<div class="orangeFrameBox">
			<div class="orangeFrameInner cf">
				<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number07.png" alt="7" /></p>
				<div class="wrap">
					<p class="last">古紙パルプへと再生処理</p>
				</div>
			</div>
	</div><!-- / .orangeFrameBox -->
	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number08.png" alt="8" /></p>
			<div class="wrap cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_flow_pds03.jpg" alt="" /></p>
				<p class="last">トイレットペーパーとして加工し、リサイクル</p>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	
	<h4 id="service03"><img src="<?php bloginfo('template_url');?>/img/service_sti03.png" alt="DS Pack Systems（ディーエスパックシステム）西川イチ押し" /></h4>
	<div class="cf">
		<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_img05.jpg" alt="" /></p>
		<p class="last">当社指定の回収用ボックスをご購入いただき、1箱から機密文書を回収させていただくサービスです。一般的な古紙リサイクルの回収の場合、1度にダンボール箱15個程度の最低回収量が設定されているため「なかなか回収に出せない」「保管スペースがない」といったケースが少なくありませんが、DS Pack Systemsならそんな心配は一切不要。お客様のニーズに合わせ、5つのタイプのサービスをご用意しています。</p>
	</div>
	
	<h5 class="capBg"><span>DS Pack Systemsの特長</span></h5>
	<p class="last"><img src="<?php bloginfo('template_url');?>/img/service_feature02.png" width="710" height="180" alt="・回収用ボックスの使用期限なし（10箱単位でご購入）！・1箱からでも回収可能！・シリアルナンバー入りなので管理が容易！・シリアルナンバー入り溶解証明を発行！" /></p>
		
	<h5 class="capBg"><span>DS Pack Systemsの流れ</span></h5>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number01.png" alt="1" /></p>
			<div class="wrap">
				<p class="last">10箱単位でDSパック（回収用ボックス）をご購入いただきます</p>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->

	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number02.png" alt="2" /></p>
			<div class="wrap">
				<p class="last">DSパックに機密文書を梱包していただきます</p>
				<ul class="asterisk">
					<li>※紙以外の禁忌品（製紙原料にならない異物）は入れないでください</li>
				</ul>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number03.png" alt="3" /></p>
			<div class="wrap">
				<p class="last">提携運送会社に回収を依頼していただきます</p>
				<ul class="asterisk">
					<li>※1箱でもたまった時点でご依頼いただけます</li>
					<li>※着払いの専用伝票をお渡しします</li>
					<li>※「すて丸くん」「BLACKBOX」は当社スタッフが回収に伺います</li>
				</ul>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number04.png" alt="4" /></p>
			<div class="wrap">
				<p class="last">提携運送会社が当社へ責任を持って運搬</p>
				<ul class="asterisk">
					<li>※「すて丸くん」「BLACKBOX」は当社スタッフが運搬いたします</li>
				</ul>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number05.png" alt="5" /></p>
			<div class="wrap">
				<p class="last">届いたDSパックを製紙会社へ運搬し溶解処理</p>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number06.png" alt="6" /></p>
			<div class="wrap">
				<p class="last">お客様へ溶解証明をお渡しします</p>
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->
	
	<h5 class="capBg"><span>各種サービスのご紹介</span></h5>
	<p class="serviceTi firstChild">すて丸くん</p>
	<div class="cf">
		<div class="wrap">
			<p class="last">シュレッダー機器のような形状の機密文書用の回収箱を使用したサービスです。当社スタッフが回収・交換にお伺いし、回収したその日のうちに未開封処理を行います。</p>
			<p class="txtLink"><a href="<?php bloginfo('url');?>/blackbox#sutemarukunSec">すて丸くんについて詳しくはこちら</a></p>
		</div>
	</div>
	
	<p class="serviceTi">BLACKBOX</p>
	<div class="cf">
		<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_serviceimg02.jpg" alt="BLACKBOX" /></p>
		<div class="wrap">
			<p class="last">専用のカギ付きハードケースを利用し、施錠したままハードケースごと回収し、当日中に処理工場へ運搬するサービスです。処理現場で解錠し溶解処理を行います。<br />容量は40kgです。</p>
					<p class="txtLink"><a href="<?php bloginfo('url');?>/blackbox#blackboxSec">BLACKBOXについて詳しくはこちら</a></p>
		</div>
	</div>
	
	<p class="serviceTi">DS Pack I（ディーエスパックI）</p>
	<div class="cf">
		<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_serviceimg03.jpg" alt="DS Pack I（ディーエスパックI）" /></p>
		<div class="wrap">
			<p class="last">これまで不可能とされていた遠隔地を対象とした回収サービスで、宅配便を使用して全国回収対応しています。<br />容量は20kgです。</p>
		</div>
	</div>
	
	<p class="serviceTi">DS Pack Express（ディーエスパックエクスプレス）</p>
	<div class="cf">
		<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/pic_boxnew.jpg" alt="DS Pack Express（ディーエスパックエクスプレス）" /></p>
		<div class="wrap">
			<p class="last">日本全国を対象とした、宅配便による廃棄文書回収サービスです。カギ付きのハードケースをご利用いただき、ケース内の中箱を未開封のまま溶解処理します。<br />容量は25kgです。</p>
		</div>
	</div>
	
  	<h4 id="service04"><img src="<?php bloginfo('template_url');?>/img/service_sti04.png" alt="Eco Police Ban（エコポリスバン）" /></h4>
	<div class="cf">
		<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_img06.jpg" alt="" /></p>
		<p class="last">大型シュレッダー搭載の特殊車両でお伺いし、お客様の目の前で廃棄書類の断裁処理を行うサービスです。処理後の紙は製紙工場へ運搬し、溶解処理を行います。</p>
	</div>
	
	<h5><img src="<?php bloginfo('template_url');?>/img/service_cap01.gif" alt="大量の保存文章が処理できます" /></h5>
	<p class="last">大量の保存文章の処理に悩みを抱えている多くの一般企業、官庁、学校などに、スピーディーで環境に配慮した処理のサービスをご提供致します。</p>
	
	<h5><img src="<?php bloginfo('template_url');?>/img/service_cap02.gif" alt="企業機密の保持（個人情報の保護）" /></h5>
	<p class="last">顧客データ、社員の個人情報、開発プロセスデータ、経理資料など、情報が流出すれば企業は致命的なダメージを受け、信用回復には労力・コスト・時間などがかかります。不要になった書類を確実に処理することは、企業にとって信頼性につながる重要な問題です。このサービスではその問題をこのバン1台で全て処理することが可能です。</p>
	
	<h5><img src="<?php bloginfo('template_url');?>/img/service_cap03.gif" alt="処理コストの削減" /></h5>
	<p class="last">「エコポリスバン」システムによる出張細断サービスは、いままでの細断のための機器購入費や廃棄処理にかかる経費・それにかかわる人件費、労務費の大幅な節約が可能になります。</p>
	
	<h5><img src="<?php bloginfo('template_url');?>/img/service_cap04.gif" alt="環境問題対策" /></h5>
	<p class="last">環境問題対策の為に、出張細断処理サービスでは、破砕した物は全て持ち帰り製紙工場にて再生紙製品としてリサイクルします。また、ISO14001認証取得・運用にもお役に立ちます。</p>
	
	<h5><img src="<?php bloginfo('template_url');?>/img/service_cap05.gif" alt="自己完結型リサイクルシステムの構築" /></h5>
	<p class="last">細断作業の所要時間は1トン～1.5トンを約１時間で処理することが可能です。<br />
				最新型シュレッダーの採用で高速処理を実現しお客様の立会い時間を短縮しております。</p>
				
	<a name="01" id="01"></a>
	<h3><img src="<?php bloginfo('template_url');?>/img/service_ti01.png" alt="西川の産業廃棄物収集処理のご紹介" /></h3>
	<p>株式会社西川では、古紙や機密文書の回収の他にもオフィスで不要になったチェアやデスクなどの什器品をはじめ、
			産廃物の廃品回収も承っております。小口の回収ももちろん可能です。
			古紙や機密文書の回収と合わせて是非ご依頼ください。</p>
	<p class="btn"><a href="<?php bloginfo('template_url');?>/img/sanpai03.pdf" target="_blank"><img src="<?php bloginfo('template_url');?>/img/service_btn_sanpai_off.png" alt="産廃許可取得一覧表はこちら(PDF)" /></a></p>
	<h4 id="service01"><img src="<?php bloginfo('template_url');?>/img/service_sti05.png" alt="産業廃棄物収集処理の流れ" /></h4>
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number01.png" alt="1" /></p>
			<div class="wrap">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_img07.jpg" alt="" width="200" height="150" /></p>
				<p class="bold">無料お見積もり</p>
						まずご連絡ください。
						当社担当者がお客様のオフィスへお伺いし、ご不用のオフィス機器及び什器備品等ヒアリングし、無料にてお見積もりを出させて頂きます。どうぞお見積書をご検討下さい。				  </div>
			</div>
	</div><!-- / .orangeFrameBox -->	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number02.png" alt="2" /></p>
				<div class="wrap cf">
					<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_img08.jpg" alt="" width="200" height="150" /></p>
					<p class="bold">ご契約締結</p>
						廃掃法に基づき、業務委託契約書を締結いたします。また回収日につきましては、打ち合わせさせて頂き手配いたします。
				</div>
		</div>
	</div><!-- / .orangeFrameBox -->	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number03.png" alt="3" /></p>
			<div class="wrap cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_img09.jpg" alt="" width="417" height="150" /></p>
				<p class="bold">回収（マニュフェストA票お渡し）</p>
					当社のスタッフが回収にお伺いいたします。回収時には、電子マニフェストで対応いたします</div>
			</div>
	</div><!-- / .orangeFrameBox -->	
	<p class="arrow"><img src="<?php bloginfo('template_url');?>/img/common/arrow_orange.png" alt="" /></p>
	
	<div class="orangeFrameBox">
		<div class="orangeFrameInner cf">
			<p class="imgL last"><img src="<?php bloginfo('template_url');?>/img/common/icon_number04.png" alt="4" /></p>
			<div class="wrap cf">
				<p class="imgR"><img src="<?php bloginfo('template_url');?>/img/service_img10.jpg" alt="" width="200" height="150" /></p>
				<p class="bold">処理完了のご通知</p>
					貴社の産業廃棄物が確実に正しく処理されたことを証明するマニュフェストB2票・D票・E票をお送りいたします。（※このマニュフェストは5年間保管ください。）
			</div>
		</div>
	</div><!-- / .orangeFrameBox -->	
	<p class="contentBnr last"><a href="<?php bloginfo('url');?>/about"><img src="<?php bloginfo('template_url');?>/img/common/bnr_about_off.png" alt="古紙回収・リサイクル業を続けて60年以上 西川が選ばれる5つの安心ポイントはこちら" /></a></p>
	<p class="contentBnr last"><a href="<?php bloginfo('url');?>/contact"><img src="<?php bloginfo('template_url');?>/img/common/contentbnr_fmail_off.jpg" alt="お電話・FAX・メールにてお気軽にお問い合わせ下さい！0120-107-254 06-6328-4800 FAX06-6327-1104 【受付】9:00～18:00（第1・3土曜日・日祝除く）　スピード無料見積 お申し込み受付中！" /></a></p>
<?php get_footer(); ?>