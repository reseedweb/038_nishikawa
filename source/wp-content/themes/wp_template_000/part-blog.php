	<div id="top-news">
		<table class="tn_table">
			<tbody>
				<?php            
					$loop = new WP_Query('showposts=5&orderby=ID&order=DESC');
					if($loop->have_posts()):
				?>
				
				<?php while($loop->have_posts()): $loop->the_post(); ?>
				<tr>
					<td class="tn_date"><?php the_time('Y/m/d'); ?></td>
					<td class="tn_title"><?php the_title(); ?></td>
				</tr>
				<?php endwhile; wp_reset_postdata();?>                        
				<?php endif; ?>  
			</tbody>
		</table>
	</div><!-- end top-news -->