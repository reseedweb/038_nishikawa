<?php get_header(); ?>
		<h3 class="firstChild"><img src="<?php bloginfo('template_url'); ?>/img/about_ti01.png" alt="西川の5つの安心ポイント" /></h3>
			<div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url'); ?>/img/about_img01.jpg" alt="" /></p>
				<p>古紙回収・リサイクル事業に特化し、大阪府・近畿地方を中心に全国にサービスを展開する株式会社西川。私たちはドライバーはじめ営業も含め、機密文書は紙としてではなくお客様の大事な情報媒体であることを心掛け、回収から処理まで一貫してサポートしております。</p>
				<p class="last">現在、お客様数は2,000社を超え、おかげさまで多くのご満足の声を頂戴しています。こちらでは、そんな当社が選ばれる5つの安心ポイントをご紹介します。</p>
			</div>
			
			
			<h4><img src="<?php bloginfo('template_url'); ?>/img/about_sti01.png" alt="その1　即お見積もりのスピード対応" /></h4>
			<div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url'); ?>/img/about_img02.jpg" alt="" /></p>
				<p class="last">お客様のご要望にいつでも対応できるよう、当社では即お見積もりのスピード対応を徹底しています。お電話・メールともにご依頼いただいてから1営業日中にお見積もりを作成。できる限りお客様をお待たせすることなく、ご要望に応じた最適なご提案を差し上げています。</p>
			</div>
			<h4><img src="<?php bloginfo('template_url'); ?>/img/about_sti02.png" alt="その2　GPS追跡・Webカメラ配信による安心の機密文書処理" /></h4>
			<div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url'); ?>/img/about_img03.jpg" alt="" /></p>
				<p class="last">回収した古紙を安全かつ確実に運搬するため、当社では運搬車両にGPS（位置情報システム）を搭載し、回収から処理場までの移動を常時監視しています。また、事前にご予約いただければ、回収時の社内での荷物の状況をインターネットを通じて、パソコンやスマートフォン(携帯電話)にて閲覧することが可能です。万が一の事故があった場合にもすぐに対応できる体制を敷き、大切な機密文書を責任を持って運搬・処理しています。</p>
				<p class="txtLink"><a href="<?php bloginfo('url'); ?>/service/blackbox">GPS追跡を使ったサービス「すて丸くん」「BLACKBOX」はこちら</a></p>
			</div>
			<h4><img src="<?php bloginfo('template_url'); ?>/img/about_sti03.png" alt="その3　創業60年以上の実績" /></h4>
			<div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url'); ?>/img/jipdec_img.jpg" alt="" /></p>
				<p class="last">当社の創業は1947年。ここ大阪市で60年以上、古紙の回収業をはじめとするリサイクル事業に取り組んでいます。また、2004年にISO14001を取得し、2005年には関西の古紙回収業者としてはじめてのPマーク取得企業となりました。昭和40年代からはじめた、個人情報などの機密情報管理のパイオニアとして、日々新技術を採り入れながら、お客様の機密文書の安全な管理・処分をお約束しています。</p>
			</div>
			<h4><img src="<?php bloginfo('template_url'); ?>/img/about_sti04.png" alt="その4　溶解処理証明書の発行" /></h4>
			<div class="cf">				
				<p class="last">お預かりした機密文書を確実に処分したことを証明するため、溶解処理証明書を発行しています。長年の付き合いのある、信頼の置ける製紙工場にて溶解処理を行っていますので、安心してお任せください。</p>
			</div>
			<h4><img src="<?php bloginfo('template_url'); ?>/img/about_sti05.png" alt="その5　補償制度完備" /></h4>
			<div class="cf">
				<p class="imgR"><img src="<?php bloginfo('template_url'); ?>/img/about_img06.jpg" alt="" /></p>
				<p class="last">お客様に安心してお任せいただくため、補償制度を完備しています。万が一機密文書の回収後に文書内容が漏洩し、お客様に損害がおよんでしまった場合、最高5億円の補償をさせていただきます。</p>
			</div>
			<p class="contentBnr last"><a href="<?php bloginfo('url'); ?>/contact"><img src="<?php bloginfo('template_url'); ?>/img/common/contentbnr_fmail_off.jpg" alt="お電話・FAX・メールにてお気軽にお問い合わせ下さい！0120-107-254 06-6328-4800 FAX06-6327-1104 【受付】9:00～18:00（第1・3土曜日・日祝除く）　スピード無料見積 お申し込み受付中！" /></a></p>
<?php get_footer(); ?>