<?php
/*
Plugin Name: Custom Area
Plugin URI: 
Description: Custom Area. Use : CustomArea::view();
Author: Dieu Pham
Author URI: 
Text Domain: custom-area
Domain Path: 
Version: 1.0
*/
/* Start Adding Functions Below this Line */
define('CUSTOM_AREA','1.0');
define('CUSTOM_AREA_PATH', dirname(__FILE__));

class CustomArea{
	private static $instance;
	public static function get_instance()
	{
        if ( null == self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
	}

	public function __construct()
	{
		add_action( 'init', array($this,'create_custom_post'), 0);
	}	

	function create_custom_post()
	{
		$this->init_post('nara','奈良','奈良','奈良');		
		$this->init_post('hyogo','兵庫県','兵庫県','兵庫県');		
		$this->init_post('kyoto','【京都府】','【京都府】','【京都府】');
		$this->init_post('wakayama','【和歌山県】','【和歌山県】','【和歌山県】');
		$this->init_post('shiga','【滋賀】','【滋賀】','【滋賀】');
		$this->init_post('osaka','大阪','大阪','大阪');
		$this->init_post('osakashi','【大阪市】','【大阪市】','【大阪市】');
		$this->init_post('kitaosaka','【北大阪】','【北大阪】','【北大阪】');
		$this->init_post('higashiosaka','【東大阪】','【東大阪】','【東大阪】');
		$this->init_post('minamikawachi','【南河内】','【南河内】','【南河内】');
		$this->init_post('sensyuu','【泉州】','【泉州】','【泉州】');		
		$this->init_post('kanagawa','【神奈川】','【神奈川】','【神奈川】');		
		$this->init_post('chiba','【千葉】','【千葉','【千葉】');		
		$this->init_post('saitama','【埼玉】','【埼玉','【埼玉】');		
		$this->init_post('ibaragi','【茨城】','【茨城','【茨城】');		
		$this->init_post('tokyo','【東京】','【東京】','【東京】');			
		$this->init_post('other','【その他】','【その他】','【その他】');		
	}

	public function init_post($post_type,$label,$all_label, $description) {

		$labels = array(
			'name'                => _x( $label, 'Post Type General Name', 'text_domain' ),
			'singular_name'       => _x( $label, 'Post Type Singular Name', 'text_domain' ),
			'menu_name'           => __( $label, 'text_domain' ),
			'parent_item_colon'   => __( $label . ' parent', 'text_domain' ),
			'all_items'           => __( 'All ' . $label, 'text_domain' ),
			'view_item'           => __( 'View ' ),
			'add_new_item'        => __( 'Add ' . $label, 'text_domain' ),
			'add_new'             => __( 'Add new ' . $label, 'text_domain' ),
			'edit_item'           => __( 'Edit ' . $label, 'text_domain' ),
			'update_item'         => __( 'Update ' . $label, 'text_domain' ),
			'search_items'        => __( 'Search ' . $label, 'text_domain' ),
			'not_found'           => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'  => __( 'Not found in trash', 'text_domain' ),
		);
		$args = array(
			'label'               => __( $all_label, 'text_domain' ),
			'description'         => __( $description, 'text_domain' ),
			'labels'              => $labels,
			'supports'            => array(  'title', 'editor' ),			
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			//'menu_position'       => 5,
			//'menu_icon'           => 'post',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);
		register_post_type( $post_type, $args );
	}	

	public static function view()
	{
	?>
	<div class="eigyouarea" >	    
		<div id="accordion">		
			<h4 class="sub-title">京都府<span>(クリックで詳細表示)</span></h4>
			<dl id="area-content">
				<dd>
					<p><?php $loop = new WP_Query(array("post_type" => "kyoto", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>、
					<?php endwhile; ?></p>
				</dd>
			</dl>
			
			<h4 class="sub-title">和歌山県<span>(クリックで詳細表示)</span></h4>
			<dl id="area-content">
				<dd>
					<p><?php $loop = new WP_Query(array("post_type" => "wakayama", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>、
					<?php endwhile; ?></p>
				</dd>
			</dl>	

			<h4 class="sub-title">滋賀<span>(クリックで詳細表示)</span></h4>
			<dl id="area-content">
				<dd>
					<p><?php $loop = new WP_Query(array("post_type" => "shiga", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>、
					<?php endwhile; ?></p>
				</dd>
			</dl>	
			
			<h4 class="sub-title">大阪<span>(クリックで詳細表示)</span></h4>
			<dl id="area-content">
				<dt>大阪市</dt>
				<dd>
					<p><?php $loop = new WP_Query(array("post_type" => "osakashi", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>、
					<?php endwhile; ?></p>
				</dd>
				<dt>北大阪</dt>
				<dd>
					<p><?php $loop = new WP_Query(array("post_type" => "kitaosaka", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>、
					<?php endwhile; ?></p>
				</dd>
				<dt>東大阪</dt>
				<dd>
					<P><?php $loop = new WP_Query(array("post_type" => "higashiosaka", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>、
					<?php endwhile; ?></P>
				</dd>

				<dt>南河内</dt>
				<dd>
					<p><?php $loop = new WP_Query(array("post_type" => "minamikawati", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>、
					<?php endwhile; ?>
					</p>
				</dd>

				<dt>泉州</dt>
				<dd><p>
					<?php $loop = new WP_Query(array("post_type" => "sensyuu", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>、
					<?php endwhile; ?>
				</p>
				</dd>
			</dl>
			
			<h4 class="sub-title">神奈川<span>(クリックで詳細表示)</span></h4>
			<dl id="area-content">
				<dd>
					<p><?php $loop = new WP_Query(array("post_type" => "kanagawa", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>、
					<?php endwhile; ?></p>
				</dd>
			</dl>

			<h4 class="sub-title">千葉<span>(クリックで詳細表示)</span></h4>
			<dl id="area-content">
				<dd>
					<p><?php $loop = new WP_Query(array("post_type" => "chiba", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>、
					<?php endwhile; ?></p>
				</dd>
			</dl>
			
			<h4 class="sub-title">埼玉<span>(クリックで詳細表示)</span></h4>
			<dl id="area-content">
				<dd>
					<p><?php $loop = new WP_Query(array("post_type" => "saitama", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>、
					<?php endwhile; ?></p>
				</dd>
			</dl>
			
			<h4 class="sub-title">茨城<span>(クリックで詳細表示)</span></h4>
			<dl id="area-content">
				<dd>
					<p><?php $loop = new WP_Query(array("post_type" => "ibaragi", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>、
					<?php endwhile; ?></p>
				</dd>
			</dl>
				
			<h4 class="sub-title">東京<span>(クリックで詳細表示)</span></h4>
			<dl id="area-content">
				<dt>東京23区</dt>
				<dd>
					<p><?php $loop = new WP_Query(array("post_type" => "tokyo", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>、
					<?php endwhile; ?></p>
				</dd>
				<dt>その他</dt>
				<dd>
					<p><?php $loop = new WP_Query(array("post_type" => "other", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>、
					<?php endwhile; ?></p>
				</dd>				
			</dl>
			
		</div>	   		
		
	</div>
	<?php
	}
}
CustomArea::get_instance();